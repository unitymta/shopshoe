<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'user_id' => 11,
                'user_name' => 'user 1',
                'password' => bcrypt('password1'),
                'email' => 'tranducsang.mta@gmail.com',
                'given_name' => 'duc sang',
                'address' => Str::random(10),
                'mobile_phone' => mt_rand(1234567899, 9999999999),
                'description' => Str::random(50),
                'retailer_id' => 1234,
                'birthday' => date('Y-m-d', mt_rand(1, 2000000000))
            ],
            [
                'user_id' => 12,
                'user_name' => 'user 2',
                'password' => bcrypt('password2'),
                'email' => 'tranducsang.unity@gmail.com',
                'given_name' => 'unityMTA',
                'address' => Str::random(10),
                'mobile_phone' => mt_rand(1234567899, 9999999999),
                'description' => Str::random(50),
                'retailer_id' => 1234,
                'birthday' => date('Y-m-d', mt_rand(1, 2000000000))
            ],
            [
                'user_id' => 13,
                'user_name' => 'user 3',
                'password' => bcrypt('password3'),
                'email' => 'tranducsang.hvktqs@gmail.com',
                'given_name' => Str::random(10),
                'address' => Str::random(10),
                'mobile_phone' => mt_rand(1234567899, 9999999999),
                'description' => Str::random(50),
                'retailer_id' => 1234,
                'birthday' => date('Y-m-d', mt_rand(1, 2000000000))
            ]
        ]);
    }
}
