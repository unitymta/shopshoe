<?php

use Illuminate\Database\Seeder;

class PurchaseOrderDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('purchase_order_details')->insert([
            [
                'purchase_id' => 11,
                'product_id' => 11,
                'product_code' => 'SP000001',
                'product_name' => 'product 1',
                'quantity' => 10,
                'price' => 100000
            ],
            [
                'purchase_id' => 12,
                'product_id' => 12,
                'product_code' => 'SP000002',
                'product_name' => 'product 2',
                'quantity' => 10,
                'price' => 120000
            ],
            [
                'purchase_id' => 11,
                'product_id' => 13,
                'product_code' => 'SP000003',
                'product_name' => 'product 3',
                'quantity' => 10,
                'price' => 160000
            ]
        ]);
    }
}
