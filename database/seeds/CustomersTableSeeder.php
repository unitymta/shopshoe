<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            [
                'customer_id' => 11,
                'code' => 'KH000001',
                'name' => Str::random(10),
                'gender' => rand(0, 1),
                'birthday' => date('Y-m-d', mt_rand(1, 2000000000)),
                'contact_number' => mt_rand(1234567899, 9999999999),
                'address' => Str::random(10),
                'email' => Str::random(10) . '@gmail.com',
                'organization' => Str::random(50),
                'comment' => Str::random(50),
                'tax_code' => Str::random(50),
                'total_invoiced' => 1000000,
                'total_point' => 500000,
                'total_revenue' => 1000000,
                'retailer_id' => 1234
            ],
            [
                'customer_id' => 12,
                'code' => 'KH000002',
                'name' => Str::random(10),
                'gender' => rand(0, 1),
                'birthday' => date('Y-m-d', mt_rand(1, 2000000000)),
                'contact_number' => mt_rand(1234567899, 9999999999),
                'address' => Str::random(10),
                'email' => Str::random(10) . '@gmail.com',
                'organization' => Str::random(50),
                'comment' => Str::random(50),
                'tax_code' => Str::random(50),
                'total_invoiced' => 1000000,
                'total_point' => 500000,
                'total_revenue' => 1000000,
                'retailer_id' => 1234
            ],
            [
                'customer_id' => 13,
                'code' => 'KH000003',
                'name' => Str::random(10),
                'gender' => rand(0, 1),
                'birthday' => date('Y-m-d', mt_rand(1, 2000000000)),
                'contact_number' => mt_rand(1234567899, 9999999999),
                'address' => Str::random(10),
                'email' => Str::random(10) . '@gmail.com',
                'organization' => Str::random(50),
                'comment' => Str::random(50),
                'tax_code' => Str::random(50),
                'total_invoiced' => 1000000,
                'total_point' => 500000,
                'total_revenue' => 1000000,
                'retailer_id' => 1234
            ]
        ]);
    }
}
