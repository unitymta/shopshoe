<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CustomerGroupDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customer_group_details')->insert([
            [
                'customer_group_detail_id' => 11,
                'customer_id' => 11,
                'group_id' => 11
            ]
        ]);
    }
}
