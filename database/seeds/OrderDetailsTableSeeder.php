<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class OrderDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_details')->insert([
            [
                'order_id' => 12,
                'product_id' => 12,
                'price' => 33232,
                'product_name' => Str::random(10),
                'product_code' => 'SP000001',
                'quantity' => 21
            ]
        ]);
    }
}
