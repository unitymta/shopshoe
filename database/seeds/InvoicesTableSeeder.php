<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class InvoicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('invoices')->insert([
            [
                'order_id' => 11,
                'invoice_id' => 11,
                'uuid' => 11,
                'code' => 'HD000001',
                'purchase_date' => date('Y-m-d', mt_rand(1, 2000000000)),
                'branch_id' => 11,
                'branch_name' => Str::random(10),
                'sold_by_id' => 11,
                'sold_by_name' => Str::random(10),
                'customer_id' => 11,
                'customer_code' => 'KH000001',
                'customer_name' => Str::random(10),
                'total' => 6000000,
                'total_payment' => 6000000,
                'discount' => 0,
                'status' => mt_rand(0,4),
                'status_value' => Str::random(10),
                'using_code' => mt_rand(0,1)
            ]
        ]);
    }
}
