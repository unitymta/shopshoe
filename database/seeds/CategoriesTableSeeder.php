<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'category_id' => 11,
                'name' => 'category 1',
                'has_child' => true,
                'retailer_id' => 1234
            ],
            [
                'category_id' => 12,
                'name' => 'category 2',
                'has_child' => false,
                'retailer_id' => 1234
            ],
            [
                'category_id' => 13,
                'name' => 'category 3',
                'has_child' => false,
                'retailer_id' => 1234
            ]
        ]);
    }
}
