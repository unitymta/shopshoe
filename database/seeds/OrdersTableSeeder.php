<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            [
                'order_id' => 11,
                'code' => 'HD000047',
                'purchase_date' => date('Y-m-d', mt_rand(1, 2000000000)),
                'branch_id' => 11,
                'branch_name' => Str::random(10),
                'sold_by_id' => 11,
                'sold_by_name' => 'name sangtx',
                'customer_code' => 'KH000001',
                'customer_name' => 'customer name',
                'total' => 1000000,
                'total_payment' => 1000000,
                'discount' => 0,
                'status' => mt_rand(0, 4),
                'retailer_id' => 1234,
                'using_code' => mt_rand(0, 1)
            ]
        ]);
    }
}
