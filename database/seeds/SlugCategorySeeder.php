<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SlugCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('slug_category')->insert([
            [
                'category_id' => 11,
                'slug_text' => 'category-nike'
            ],
            [
                'category_id' => 12,
                'slug_text' => 'category-adidas'
            ],
            [
                'category_id' => 13,
                'slug_text' => 'category-converse'
            ]
        ]);
    }
}
