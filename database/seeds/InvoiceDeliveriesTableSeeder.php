<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class InvoiceDeliveriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('invoice_deliveries')->insert([
            [
                'invoice_id' => 11,
                'service_type' => '',
                'service_type_text' => '',
                'status' => mt_rand(1, 4),
                'status_value' => Str::random(10),
                'receiver' => Str::random(10),
                'contact_number' => mt_rand(1111111111,9999999999),
                'address' => Str::random(10),
                'location_id' => mt_rand(111, 444),
                'location_name' => Str::random(10)
            ]
        ]);
    }
}
