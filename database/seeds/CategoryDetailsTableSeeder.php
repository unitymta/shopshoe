<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_details')->insert([
            [
                'parent_id' => 11,
                'category_id' => 21,
                'name' => 'category 21',
                'retailer_id' => 1234
            ],
            [
                'parent_id' => 11,
                'category_id' => 22,
                'name' => 'category 22',
                'retailer_id' => 1234
            ],
            [
                'parent_id' => 11,
                'category_id' => 23,
                'name' => 'category 23',
                'retailer_id' => 1234
            ]
        ]);
    }
}
