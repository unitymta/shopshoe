<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CustomerGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customer_groups')->insert([
            [
                'group_id' => 11,
                'name' => Str::random(10),
                'description' => Str::random(50),
                'retailer_id' => 1234,
                'create_by_id' => 11
            ]
        ]);
    }
}
