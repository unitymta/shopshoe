<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BranchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('branches')->insert([
            [
                'branch_id' => 11,
                'branch_name' => 'chi nhanh 1',
                'email' => Str::random(10) . '@gmail.com',
                'address' => Str::random(10),
                'contact_number' => mt_rand(1234567899, 9999999999),
                'retailer_id' => 1234
            ],
            [
                'branch_id' => 12,
                'branch_name' => 'chi nhanh 2',
                'email' => Str::random(10) . '@gmail.com',
                'address' => Str::random(10),
                'contact_number' => mt_rand(1234567899, 9999999999),
                'retailer_id' => 1234
            ],
            [
                'branch_id' => 13,
                'branch_name' => 'chi nhanh 3',
                'email' => Str::random(10) . '@gmail.com',
                'address' => Str::random(10),
                'contact_number' => mt_rand(1234567899, 9999999999),
                'retailer_id' => 1234
            ]
        ]);
    }
}
