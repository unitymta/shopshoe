<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SlugsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('slugs')->insert([
            [
                'product_id' => 11,
                'slug_text' => 'product-1'
            ],
            [
                'product_id' => 12,
                'slug_text' => 'product-2'
            ],
            [
                'product_id' => 13,
                'slug_text' => 'product-3'
            ],
            [
                'product_id' => 14,
                'slug_text' => 'product-4'
            ],
            [
                'product_id' => 15,
                'slug_text' => 'product-5'
            ],
            [
                'product_id' => 16,
                'slug_text' => 'product-6'
            ],
            [
                'product_id' => 17,
                'slug_text' => 'product-7'
            ],
            [
                'product_id' => 18,
                'slug_text' => 'product-8'
            ],
            [
                'product_id' => 19,
                'slug_text' => 'product-9'
            ],
            [
                'product_id' => 20,
                'slug_text' => 'product-10'
            ],
            [
                'product_id' => 21,
                'slug_text' => 'product-11'
            ],
            [
                'product_id' => 22,
                'slug_text' => 'product-12'
            ],
            [
                'product_id' => 23,
                'slug_text' => 'product-13'
            ],
            [
                'product_id' => 24,
                'slug_text' => 'product-14'
            ],
            [
                'product_id' => 25,
                'slug_text' => 'product-15'
            ],
            [
                'product_id' => 26,
                'slug_text' => 'product-16'
            ],
            [
                'product_id' => 27,
                'slug_text' => 'product-17'
            ],
            [
                'product_id' => 28,
                'slug_text' => 'product-18'
            ],
        ]);
    }
}
