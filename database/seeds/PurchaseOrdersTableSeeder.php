<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PurchaseOrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('purchase_orders')->insert([
            [
                'purchase_id' => 11,
                'code' => 'NH000047',
                'branch_id' => 11,
                'branch_name' => 'Thọ Xuân',
                'purchase_date' => '2019-05-10 00:00:00',
                'discount_type' => 2,
                'total' => 1000000,
                'supplier_id' => 11,
                'supplier_name' => 'NCC001',
                'partner_name' => 'Nguoi giao hang nhap',
                'purchase_by_id' => 11,
                'purchase_name' => 'duc sang'
            ],
            [
                'purchase_id' => 12,
                'code' => 'NH000047',
                'branch_id' => 11,
                'branch_name' => 'Thọ Xuân',
                'purchase_date' => '2019-05-10 00:00:00',
                'discount_type' => 2,
                'total' => 1200000,
                'supplier_id' => 11,
                'supplier_name' => 'NCC001',
                'partner_name' => 'Nguoi giao hang nhap',
                'purchase_by_id' => 11,
                'purchase_name' => 'duc sang'
            ],
            [
                'purchase_id' => 13,
                'code' => 'NH000047',
                'branch_id' => 11,
                'branch_name' => 'Thọ Xuân',
                'purchase_date' => '2019-05-10 00:00:00',
                'discount_type' => 2,
                'total' => 1600000,
                'supplier_id' => 11,
                'supplier_name' => 'NCC001',
                'partner_name' => 'Nguoi giao hang nhap',
                'purchase_by_id' => 11,
                'purchase_name' => 'duc sang'
            ]
        ]);
    }
}
