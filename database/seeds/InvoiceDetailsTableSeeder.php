<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class InvoiceDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('invoice_details')->insert([
            [
                'invoice_id' => 11,
                'product_id' => 11,
                'product_code' => 'SP000001',
                'product_name' => Str::random(10),
                'quantity' => mt_rand(1, 100),
                'price' => 1111111,
                'discount' => 0,
                'discount_ratio' => 0,
                'use_point' => mt_rand(0, 1),
                'sub_total' => 0,
                'serial_number' => ''
            ]
        ]);
    }
}
