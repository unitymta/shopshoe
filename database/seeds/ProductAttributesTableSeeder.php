<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductAttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_attributes')->insert([
            [
                'product_id' => 11,
                'sku' => 'SP000001-01',
                'size' => '40',
                'color' => 'đen',
                'quantity' => 2
            ],
            [
                'product_id' => 11,
                'sku' => 'SP000001-01',
                'size' => '41',
                'color' => 'đen',
                'quantity' => 2
            ],
            [
                'product_id' => 11,
                'sku' => 'SP000001-02',
                'size' => '41',
                'color' => 'đỏ',
                'quantity' => 1
            ],
            [
                'product_id' => 11,
                'sku' => 'SP000001-03',
                'size' => '42',
                'color' => 'trắng',
                'quantity' => 2
            ],
            [
                'product_id' => 11,
                'sku' => 'SP000001-02',
                'size' => '42',
                'color' => 'đỏ',
                'quantity' => 2
            ],
            [
                'product_id' => 11,
                'sku' => 'SP000001-02',
                'size' => '43',
                'color' => 'đỏ',
                'quantity' => 2
            ]
        ]);
    }
}
