<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'product_id' => 26,
                'retailer_id' => 1234,
                'code' => 'SP000001',
                'name' => Str::random(10),
                'full_name' => Str::random(15),
                'description' => Str::random(40),
                'images' => Str::random(20).'.png',
                'category_id'=>11,
                'category_name' => Str::random(40),
                'allow_sale' => mt_rand(0,1),
                'has_variant' => mt_rand(0,1),
                'price_base' => 150000,
                'price_cost' => 120000,
                'unit'=> 'chiec',
                'branch_id' => 11,
                'branch_name' => Str::random(10)
            ],
            [
                'product_id' => 27,
                'retailer_id' => 1234,
                'code' => 'SP000002',
                'name' => Str::random(10),
                'full_name' => Str::random(15),
                'description' => Str::random(40),
                'images' => Str::random(20).'.png',
                'category_id'=>12,
                'category_name' => Str::random(40),
                'allow_sale' => mt_rand(0,1),
                'has_variant' => mt_rand(0,1),
                'price_base' => 100000,
                'price_cost' => 90000,
                'unit'=> 'chiec',
                'branch_id' => 11,
                'branch_name' => Str::random(10)
            ],
            [
                'product_id' => 28,
                'retailer_id' => 1234,
                'code' => 'SP000003',
                'name' => Str::random(10),
                'full_name' => Str::random(15),
                'description' => Str::random(40),
                'images' => Str::random(20).'.png',
                'category_id'=>11,
                'category_name' => Str::random(40),
                'allow_sale' => mt_rand(0,1),
                'has_variant' => mt_rand(0,1),
                'price_base' => 250000,
                'price_cost' => 220000,
                'unit'=> 'cai',
                'branch_id' => 11,
                'branch_name' => Str::random(10)
            ]
        ]);
    }
}
