<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payments')->insert([
            [
                'invoice_id' => 11,
                'payment_id' => 11,
                'code' => 'SP000001',
                'amount' => 200000,
                'method' => '',
                'status' => mt_rand(0, 4),
                'status_value' => Str::random(15),
                'trans_date' => date('Y-m-d', mt_rand(1, 2000000000))
            ]
        ]);
    }
}
