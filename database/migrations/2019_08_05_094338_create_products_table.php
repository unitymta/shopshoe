<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned()->unique();
            $table->integer('retailer_id')->unsigned();
            $table->string('code');
            $table->string('name')->nullable();
            $table->string('full_name')->nullable();
            $table->string('description')->nullable();
            $table->string('images')->nullable();
            $table->integer('category_id')->unsigned();
            $table->string('category_name')->nullable();
            $table->boolean('allow_sale')->nullable();
            $table->boolean('has_variant')->nullable();
            $table->integer('price_base')->unsigned();
            $table->integer('price_cost')->unsigned();
            $table->string('unit')->nullable();
            $table->boolean('is_active')->default(true)->nullable();
            $table->string('oder_template')->nullable();
            $table->integer('branch_id')->unsigned()->nullable();
            $table->string('branch_name')->nullable();
            $table->integer('inventory')->default(0)->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
