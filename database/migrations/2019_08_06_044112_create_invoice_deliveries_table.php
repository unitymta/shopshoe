<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_deliveries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invoice_id')->unsigned();
            $table->string('service_type');
            $table->string('service_type_text');
            $table->integer('status')->unsigned();
            $table->string('status_value');
            $table->string('receiver');
            $table->string('contact_number');
            $table->string('address');
            $table->integer('location_id')->unsigned();
            $table->string('location_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_deliveries');
    }
}
