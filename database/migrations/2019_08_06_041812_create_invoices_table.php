<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('invoice_id')->unique()->unsigned();
            $table->integer('uuid')->unsigned();
            $table->string('code');
            $table->dateTime('purchase_date');
            $table->integer('branch_id')->unsigned();
            $table->string('branch_name');
            $table->integer('sold_by_id')->unsigned();
            $table->string('sold_by_name');
            $table->integer('customer_id')->unsigned();
            $table->string('customer_code');
            $table->string('customer_name');
            $table->integer('total')->unsigned();
            $table->integer('total_payment')->unsigned();
            $table->integer('discount')->unsigned();
            $table->integer('status')->unsigned();
            $table->string('status_value');
            $table->boolean('using_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
