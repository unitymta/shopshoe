<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->foreign('category_id')->references('category_id')->on('categories');
            $table->foreign('branch_id')->references('branch_id')->on('branches');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->foreign('branch_id')->references('branch_id')->on('branches');
            $table->foreign('sold_by_id')->references('user_id')->on('users');
            $table->foreign('customer_code')->references('code')->on('customers');
        });
        Schema::table('order_details', function (Blueprint $table) {
            $table->foreign('order_id')->references('order_id')->on('orders');
            $table->foreign('product_id')->references('product_id')->on('products');
        });
        Schema::table('customer_groups', function (Blueprint $table) {
            $table->foreign('create_by_id')->references('user_id')->on('users');
        });
        Schema::table('customer_group_details', function (Blueprint $table) {
            $table->foreign('customer_id')->references('customer_id')->on('customers');
            $table->foreign('group_id')->references('group_id')->on('customer_groups');
        });
        Schema::table('invoices', function (Blueprint $table) {
            $table->foreign('order_id')->references('order_id')->on('orders');
            $table->foreign('branch_id')->references('branch_id')->on('branches');
            $table->foreign('sold_by_id')->references('user_id')->on('users');
        });
        Schema::table('invoice_deliveries', function (Blueprint $table) {
            $table->foreign('invoice_id')->references('invoice_id')->on('invoices');
        });
        Schema::table('invoice_details', function (Blueprint $table) {
            $table->foreign('invoice_id')->references('invoice_id')->on('invoices');
            $table->foreign('product_id')->references('product_id')->on('products');
        });
        Schema::table('payments', function (Blueprint $table) {
            $table->foreign('invoice_id')->references('invoice_id')->on('invoices');
        });
        Schema::table('category_details', function (Blueprint $table) {
            $table->foreign('parent_id')->references('category_id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign('products_category_id_foreign');
            $table->dropForeign('products_branch_id_foreign');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign('orders_branch_id_foreign');
            $table->dropForeign('orders_sold_by_id_foreign');
            $table->dropForeign('orders_customer_code_foreign');
        });
        Schema::table('order_details', function (Blueprint $table) {
            $table->dropForeign('order_details_order_id_foreign');
            $table->dropForeign('order_details_product_id_foreign');
        });
        Schema::table('customer_groups', function (Blueprint $table) {
            $table->dropForeign('customer_groups_create_by_id_foreign');
        });
        Schema::table('customer_group_details', function (Blueprint $table) {
            $table->dropForeign('customer_group_details_customer_id_foreign');
            $table->dropForeign('customer_group_details_group_id_foreign');
        });
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropForeign('invoices_order_id_foreign');
            $table->dropForeign('invoices_branch_id_foreign');
            $table->dropForeign('invoices_sold_by_id_foreign');
        });
        Schema::table('invoice_deliveries', function (Blueprint $table) {
            $table->dropForeign('invoice_deliveries_invoice_id_foreign');
        });
        Schema::table('invoice_details', function (Blueprint $table) {
            $table->dropForeign('invoice_details_invoice_id_foreign');
            $table->dropForeign('invoice_details_product_id_foreign');
        });
        Schema::table('payments', function (Blueprint $table) {
            $table->dropForeign('payments_invoice_id_foreign');
        });
        Schema::table('category_details', function (Blueprint $table) {
            $table->dropForeign('category_details_parent_id_foreign');
        });
    }
}
