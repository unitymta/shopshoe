<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchase_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->string('product_code');
            $table->string('product_name');
            $table->integer('quantity')->unsigned();
            $table->integer('price')->unsigned();
            $table->decimal('discount')->nullable();
            $table->foreign('purchase_id')->references('purchase_id')->on('purchase_orders');
            $table->foreign('product_id')->references('product_id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_order_details', function (Blueprint $table) {
            $table->dropForeign('purchase_order_details_purchase_id_foreign');
            $table->dropForeign('purchase_order_details_product_id_foreign');
        });
        Schema::dropIfExists('purchase_order_details');
    }
}
