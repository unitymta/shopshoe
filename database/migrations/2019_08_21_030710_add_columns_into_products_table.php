<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsIntoProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->integer('discount_type')->comment('1: percent, 2:VND')->after('allow_sale')->unsigned()->nullable();
            $table->decimal('discount')->after('discount_type')->nullable();
            $table->integer('quantity')->after('inventory')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('products', 'discount_type')) {
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('discount_type');
            });
        }
        if (Schema::hasColumn('products', 'discount')) {
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('discount');
            });
        }
        if (Schema::hasColumn('products', 'quantity')) {
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('quantity');
            });
        }
    }
}
