<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchase_id')->unique()->unsigned();
            $table->string('code');
            $table->integer('branch_id')->unsigned();
            $table->string('branch_name');
            $table->dateTime('purchase_date');
            $table->integer('discount_type')->comment('1:percent, 2:VND')->unsigned();
            $table->integer('total')->unsigned();
            $table->integer('supplier_id')->unsigned();
            $table->string('supplier_name');
            $table->string('partner_name');
            $table->integer('purchase_by_id')->unsigned();
            $table->string('purchase_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
