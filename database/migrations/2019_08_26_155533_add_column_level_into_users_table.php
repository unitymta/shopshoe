<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnLevelIntoUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('level')->default(3)->unsigned()->comment('1:supperAdmin 2:admin 3:user');
            $table->string('user_name')->nullable()->change();
            $table->string('given_name')->nullable()->change();
            $table->string('address')->nullable()->change();
            $table->string('mobile_phone')->nullable()->change();
            $table->string('description')->nullable()->change();
            $table->integer('retailer_id')->nullable()->change();
            $table->date('birthday')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('users', 'level')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('level');
            });
        }
    }
}
