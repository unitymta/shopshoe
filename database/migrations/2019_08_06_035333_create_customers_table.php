<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unique()->unsigned();
            $table->string('code')->unique();
            $table->string('name');
            $table->boolean('gender');
            $table->date('birthday');
            $table->string('contact_number');
            $table->string('address')->nullable();
            $table->string('location_name')->nullable();
            $table->string('email');
            $table->string('organization')->nullable();
            $table->string('comment')->nullable();
            $table->string('tax_code')->nullable();
            $table->integer('debt')->unsigned()->nullable();
            $table->integer('total_invoiced')->unsigned();
            $table->integer('total_point')->unsigned();
            $table->integer('total_revenue')->unsigned();
            $table->integer('retailer_id')->unsigned();
            $table->string('reward_point')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
