<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_id')->unique()->unsigned();
            $table->integer('invoice_id')->unsigned();
            $table->string('code');
            $table->integer('amount')->unsigned();
            $table->string('method');
            $table->integer('status')->unsigned();
            $table->string('status_value');
            $table->dateTime('trans_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
