<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Giày Việt xuất khẩu, giá hấp dẫn, chất lượng cao"/>
    <meta name="description" content="Giày Việt xuất khẩu, giá hấp dẫn, chất lượng cao"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0"/>
    <meta property="og:title" content="{{$productDetail->full_name}}"/>
    <meta property="og:site_name" content="unitymta.com"/>

    <title>{{$productDetail->full_name}}</title>

    <link rel="canonical" href="/"/>
    <link rel="shortcut icon" href="/images/icon/favicon_logo.png"/>
    <link class="theme-selector" rel="stylesheet" type="text/css" href="/css/app.css" media="all"/>
    <link class="theme-selector" rel="stylesheet" type="text/css" href="/css/customcss.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="/css/style.css" media="all"/>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,300' rel='stylesheet' type='text/css'>
    <link class="theme-selector" rel="stylesheet" type="text/css" href="/css/mobileresponsive.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="/css/netoTicker.css" media="all"/>
    <link rel="stylesheet" type="text/css"
          href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui-1.8.18.custom.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="/css/jquery.fancybox.css" media="all"/>

    <!--[if lte IE 8]>
    <script type="text/javascript" src="/js/html5shiv.js"></script>
    <script type="text/javascript" src="/js/respond.min.js"></script>
    <![endif]-->

</head>
<body id="n_product">

@include('block.header')

<div id="main-content" class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12">

            @include('block.assurance')

            <ul class="breadcrumb">
                <li>
                    <a href="/" title="Home">Home</a>
                </li>
                <li>
                    <a href="/categories/{{$slugCategory}}"
                       title="{{$productDetail->category_name}}">{{$productDetail->category_name}}</a>
                </li>
                <li>
                    <a href="{{route('detail', ['slug'=>$slug, 'product_id' => $productDetail->product_id])}}">{{$productDetail->full_name}}</a>
                </li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 box-relative">
            <div class="categorypage">
                <div class="promotioncatepage">
                    <div class="wrapper-assurance">
                        <div class="row">
                            <div class="col-xs-6 col-sm-2">
                                <div class="thumbnail-assurance">
                                    <div class="image">
                                        <img src="/images/icon/35.png" alt="Chất lượng và Chính hãng" border="0">
                                    </div>
                                    <div class="caption-assurance">
                                        <h5 class="headline">Chất lượng</h5>
                                        <p>Đảm bảo tuyệt đối</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-2">
                                <div class="thumbnail-assurance">
                                    <div class="image">
                                        <img src="/images/icon/43.png" alt="Đổi trả 7 ngày" border="0">
                                    </div>
                                    <div class="caption-assurance">
                                        <h5 class="headline">Đổi trả</h5>
                                        <p>Miễn phí trong 7 ngày</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-2">
                                <div class="thumbnail-assurance">
                                    <div class="image">
                                        <img src="/images/icon/44.png" alt="Chuyển phát nhanh" border="0">
                                    </div>
                                    <div class="caption-assurance">
                                        <h5 class="headline">Chuyển phát</h5>
                                        <p>Nhanh, tiết kiệm</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-2">
                                <div class="thumbnail-assurance">
                                    <div class="image">
                                        <img src="/images/icon/45.png" alt="Việt Nam" border="0">
                                    </div>
                                    <div class="caption-assurance">
                                        <h5 class="headline">Phạm vi</h5>
                                        <p>Toàn Việt Nam</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-2">
                                <div class="thumbnail-assurance">
                                    <div class="image">
                                        <img src="/images/icon/46.png" alt="Hậu mãi" border="0">
                                    </div>
                                    <div class="caption-assurance">
                                        <h5 class="headline">Hậu mãi</h5>
                                        <p>Tận tình, chu đáo</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="catepage-rightslidebar">
                        <ul class="list-inline list-social">
                            <li>
                                <div class="payment-icon-container">
                                    <div class="payment-icon payment-icon-mastercard"></div>
                                </div>
                            </li>
                            <li>
                                <div class="payment-icon-container">
                                    <div class="payment-icon payment-icon-visa"></div>
                                </div>
                            </li>
                            <li>
                                <div class="payment-icon-container">
                                    <div class="payment-icon payment-icon-paypal"></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="custompagerow2">
                    <div id="multiitemadd" class="row custompagerow">
                        <div class="col-sm-5">
                            <div id="_jstl__images">
                                <div id="_jstl__images_r">
                                    <div class="before-images-detail-product mb">
                                        <h2 class="name-product" itemprop="name">{{$productDetail->full_name}}</h2>
                                        <h4 class="text-muted">Còn hàng</h4>
                                    </div>
                                    <div class="brandintopright">
                                        <p>
                                            <a title="{{$productDetail->full_name}}" href="#"><img
                                                    class="img-responsive" src=""></a>
                                        </p>
                                    </div>
                                    <div class="main-image text-center">
                                        <a href="#" class=" fancybox">
                                            <div class="zoom" style="position: relative; overflow: hidden;">
                                                <img src="/images/{{$productDetail->images}}" class="hidden">
                                                <img src="/images/{{$productDetail->images}}"
                                                     alt="{{$productDetail->full_name}}" border="0" id="main-image"
                                                     itemprop="image">
                                                <img src="/images/{{$productDetail->images}}" class="zoomImg"
                                                     style="position: absolute; top: -216.857px; left: -337.418px; opacity: 0; width: 800px; height: 800px; border: none; max-width: none; max-height: none;">
                                            </div>
                                        </a>
                                        <a href="#" class="brand-image hidden"><img
                                                src="/images/{{$productDetail->images}}"/></a>
                                    </div>
                                    <div class="clear"></div>
                                    <br/>
                                    <div class="clear"></div>
                                    <div class="row thumb-list-prd">
                                        <div class="col-xs-3">
                                            <a href="#" class="fancybox" rel="product_images" title="Large View">
                                                <img src="/images/{{$productDetail->images}}" border="0"
                                                     class="img-responsive product-image-small" title="Large View">
                                            </a>
                                        </div>
                                        <div class="col-xs-3">
                                            <a href="#" class="fancybox" rel="product_images" title="Large View">
                                                <img src="/images/{{$productDetail->images}}" border="0"
                                                     class="img-responsive product-image-small" title="Large View">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-7 rightside">
                            <div id="_jstl__header">
                                <div id="_jstl__header_r">
                                    <div class="row">
                                        <div class="wrapper-product-title col-sm-8 dsk">
                                            <div class="before-images-detail-product">
                                                <h2 class="name-product"
                                                    itemprop="name">{{$productDetail->full_name}}</h2>
                                            </div>
                                            <div class="socialmedia socialmedia-product-details">
                                                <button class="btn btn-default btn-xs dropdown-toggle" type="button"
                                                        id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                                                    <span class="hidden-xs">Share: </span>
                                                    <i class="fa fa-facebook-square text-facebook"></i>
                                                    <i class="fa fa-twitter-square text-twitter"></i>
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                                    <li role="presentation">
                                                        <a class="js-social-share" role="menuitem" tabindex="-1"
                                                           href="//www.facebook.com/sharer/sharer.php?u={{urlencode(url()->current())}}">
                                                            <i class="fa fa-facebook-square text-facebook"></i>
                                                            Facebook
                                                        </a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a class="js-social-share" role="menuitem" tabindex="-1"
                                                           href="//twitter.com/intent/tweet/?text={{urlencode($productDetail->full_name)}}&amp;url={{urlencode(url()->current())}}">
                                                            <i class="fa fa-twitter-square text-twitter"></i>
                                                            Twitter
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div itemprop="offers" class="wrapper-pricing col-sm-4">
                                            @if($productDetail->allow_sale == 1)
                                                <div class="productprice productpricetext">
                                                    {{number_format($productDetail->price_base - ($productDetail->price_base)*($productDetail->discount)/100)}}
                                                    VND
                                                </div>
                                                @if($productDetail->discount_type == 1)
                                                    <div class="productrrp text-muted">
                                                        Giá cũ <span id="convert1"
                                                                     amount="{{number_format($productDetail->price_base)}}">{{number_format($productDetail->price_base)}} VND</span>
                                                    </div>
                                                    <div class="productsave">
                                                        Giảm <span id="convert2"
                                                                   amount="{{number_format(($productDetail->price_base)*($productDetail->discount)/100)}}">{{number_format(($productDetail->price_base)*($productDetail->discount)/100)}} VND</span>
                                                        ({{number_format($productDetail->discount)}}%)
                                                    </div>
                                                @elseif($productDetail->discount_type == 2)
                                                    <span>{{number_format($productDetail->price_base - $productDetail->discount)}} VND</span>
                                                @endif
                                            @else
                                                <div class="productprice productpricetext">
                                                    {{number_format($productDetail->price_base)}} VND
                                                </div>
                                            @endif

                                            @if($productDetail->inventory == 0)
                                                <span itemprop="availability" content="in_stock"
                                                      class="label label-success out_stock">Hết hàng</span>
                                            @else
                                                <span itemprop="availability" content="in_stock"
                                                      class="label label-success">Còn hàng</span>
                                            @endif
                                        </div>
                                    </div>
                                    <form class="buying-options full-width">
                                        <input type="hidden" id="model{{$productDetail->code}}" name="model"
                                               value="{{$productDetail->full_name}}">
                                        <input type="hidden" id="thumb{{$productDetail->code}}" name="thumb"
                                               value="/images/{{$productDetail->images}}">
                                        <input type="hidden" id="sku{{$productDetail->code}}" name="sku"
                                               value="{{$productDetail->code}}">
                                        <div class="row btn-stack">
                                            <div class="col-xs-12 col-md-4 qtyclass">
                                                <button type="button" id="sub" class="sub btn btn-default">-</button>
                                                <input type="text" min="0"
                                                       class="form-control qty input-lg inline-block-imp"
                                                       id="qty{{$productDetail->product_id}}" name="qty" value="1"
                                                       size="2">
                                                <button type="button" id="add" class="add btn btn-default">+</button>
                                            </div>
                                            <div class="col-xs-12 col-md-4 addtocartclass">
                                                <button @if($productDetail->inventory == 0) disabled @endif type="button" title="Thêm vào giỏ hàng"
                                                        class="addMultipleCartItems btn btn-success btn-cart btn-block btn-lg btn-loads">
                                                    <i class="fa fa-shopping-cart icon-white"></i> Thêm vào giỏ hàng
                                                </button>
                                            </div>
                                            <div class="col-xs-12 col-md-4 wishlistclass">
                                                <div class="product-wishlist">
                                                    <a class="wishlist_toggle btn btn-primary btn-lg btn-block">
                                                        <span class="add" rel="wishlist_textRVM4151"><i
                                                                class="fa fa-star-o"></i> Thêm vào danh sách yêu thích</span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <form>
                                <input type="hidden" id="_itmspec_sku" value="{{$productDetail->code}}">
                                <div class="row">
                                    <div class="_itmspec_listopt">
                                        <div class="col-xs-12 variation-name-custom">
                                            <strong>Màu - Size</strong>
                                        </div>
                                        <div class="col-xs-12 col-md-8 specific">
                                            <input type="hidden" class="_itmspec_val" value="">
                                            <div class="n-wrapper-form-control">
                                                <span class="_itmspec_optpl">
                                                    <select class="_itmspec_opt form-control" name="chose-color">
                                                        @foreach($productAttributes as $key=>$value)
                                                            @if($value->quantity == 0)
                                                                <option value="{{$value->sku}}">{{$value->color}} - {{$value->size}} - ( Tạm hết )</option>
                                                            @else
                                                                <option
                                                                    value="{{$value->sku}}">{{$value->color}} - {{$value->size}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div id="_jstl__buying_options">
                                <input type="hidden" id="_jstl__buying_options_k0" value="template"/>
                                <input type="hidden" id="_jstl__buying_options_v0" value="YnV5aW5nX29wdGlvbnM"/>
                                <input type="hidden" id="_jstl__buying_options_k1" value="type"/>
                                <div id="_jstl__buying_options_r">
                                    <div class="extra-options">
                                        <div class="panel panel-default" id="shipbox">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Phí ship</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row btn-stack">
                                                    <div class="col-xs-12 col-md-3">
                                                        <input type="number" name="input" id="n_qty" value="0" size="2"
                                                               class="form-control" placeholder="Qty" min="0" max="99">
                                                    </div>
                                                    <div class="col-xs-12 col-md-3">
                                                        <select id="country" class="form-control">
                                                            @foreach($dataLocal as $key=>$value)
                                                                <option value="{{$value['id']}}">{{$value['name']}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3">
                                                        <input type="text" name="input" id="zip" value="11111" size="5"
                                                               class="form-control" placeholder="Post Code">
                                                    </div>
                                                    <div class="col-xs-12 col-md-3">
                                                        <button type="button"
                                                                class="btn btn-block btn-primary btn-loads"
                                                                data-loading-text="<i class='fa fa-refresh fa-spin' style='font-size: 14px'></i>"
                                                                title="Phí"><i class="fa fa-refresh"></i>
                                                            Tính phí
                                                        </button>
                                                    </div>
                                                </div>
                                                <br>
                                                <hr style="border-color: #ffffff;">
                                                <strong>Chuyển phát nhanh</strong> - 100,000 VND (Dự kiến: 2 ngày)<br>
                                                <i class="text-muted">Chuyển phát nhanh: Ước tính 1-3 ngày làm việc
                                                    (Không bao gồm cuối tuần và ngày lễ)</i>
                                                <hr style="border-color: #ffffff;">
                                                <strong>Chuyển phát tiêu chuẩn</strong> - 50,000 VND (Dự kiến: 5
                                                ngày)<br>
                                                <i class="text-muted">Chuyển phát tiêu chuẩn: Ước tính 1-7 ngày làm việc
                                                    (Không bao gồm cuối tuần và ngày lễ)</i>
                                            </div>
                                        </div>

                                        <div class="youmaylike">
                                        </div>

                                        <div class="fixed-top-price">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h3 cldss="name-product"
                                                            itemprop="name">{{$productDetail->full_name}}</h3>
                                                    </div>
                                                    <div class="col-md-3">
                                                        @if($productDetail->allow_sale == 1)
                                                            <div class="productprice productpricetext">
                                                                {{number_format($productDetail->price_base - ($productDetail->price_base)*($productDetail->discount)/100)}}
                                                                VND
                                                            </div>
                                                            @if($productDetail->discount_type == 1)
                                                                <div class="productrrp text-muted">
                                                                    Giá cũ <span id="convert1"
                                                                                 amount="{{number_format($productDetail->price_base)}}">{{number_format($productDetail->price_base)}} VND</span>
                                                                </div>
                                                                <div class="productsave">
                                                                    Giảm <span id="convert2"
                                                                               amount="{{number_format(($productDetail->price_base)*($productDetail->discount)/100)}}">{{number_format(($productDetail->price_base)*($productDetail->discount)/100)}} VND</span>
                                                                    ({{number_format($productDetail->discount)}}%)
                                                                </div>
                                                            @elseif($productDetail->discount_type == 2)
                                                                <span>{{number_format($productDetail->price_base - $productDetail->discount)}} VND</span>
                                                            @endif
                                                        @else
                                                            <div class="productprice productpricetext">
                                                                {{number_format($productDetail->price_base)}} VND
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-3">
                                                        <form class="buying-options full-width">
                                                            <input type="hidden" id="model{{$productDetail->code}}"
                                                                   name="model"
                                                                   value="{{$productDetail->full_name}}">
                                                            <input type="hidden" id="thumb{{$productDetail->code}}"
                                                                   name="thumb"
                                                                   value="/images/{{$productDetail->images}}">
                                                            <input type="hidden" id="sku{{$productDetail->code}}"
                                                                   name="sku"
                                                                   value="{{$productDetail->code}}">
                                                            <button @if($productDetail->inventory == 0) disabled @endif type="button" title="Thêm vào giỏ hàng"
                                                                    class="addtocart btn btn-success btn-cart btn-block btn-lg btn-loads"
                                                                    data-loading-text="<i class='fa fa-spinner fa-spin' style='font-size: 14px'></i>">
                                                                <i class="fa fa-shopping-cart icon-white"></i> Thêm vào giỏ hàng
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal fade notifymodal" id="notifymodal">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Thông báo cho tôi khi hàng về</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label>Họ Tên</label>
                                                        <input placeholder="SangTX" name="from_name" id="from_name"
                                                               type="text" class="form-control" value="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <input placeholder="tranducsang.mta@gmail.com" name="from"
                                                               id="from" type="email" class="form-control" value="">
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <input class="btn btn-danger" type="button" data-dismiss="modal" value="Hủy">
                                                    <input class="btn btn-success" type="button" data-dismiss="modal" value="Lưu">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row custompagerow">
                        <div class="col-xs-12">
                            <div class="tabcustom">
                                <input type="checkbox" name="tabs" id="tab1" checked>
                                <input type="checkbox" name="tabs" id="tab2">
                                <input type="checkbox" name="tabs" id="tab3">
                                <input type="checkbox" name="tabs" id="tab4">
                                <input type="checkbox" name="tabs" id="tab5">
                                <input type="checkbox" name="tabs" id="tab6">
                                <input type="checkbox" name="tabs" id="tab7" checked>
                                <h3>
                                    <label for="tab1" id="lt1">Mô tả <i class="fa fa-chevron-down" aria-hidden="true"></i></label>
                                </h3>
                                <div id="content1" class="section">
                                    <p><span style="font-size:20px;"><strong>Chức năng chính</strong></span></p>
                                    <ul>
                                        <li>
                                            <span>Chạy</span>
                                        </li>
                                        <li>
                                            <span>Tập gym</span>
                                        </li>
                                    </ul>
                                    <p>Đôi giày này là đôi đầu tiên nằm trong dòng giày mới có tên gọi là “NAMELESS EDITION” của Biti’s Hunter. Dòng giày nhằm tôn vinh những biểu tượng đương đại hiện nay trong cộng đồng nước ta. Sơn Tùng chính là đại diện đầu tiên trong lĩnh vực âm nhạc khởi đầu cho dòng giày này.</p>
                                    <p>Điều đáng chú ý nhất khi vừa nhìn thấy đôi giày này đó chính là kiểu dáng “chunky sneakers” – một xu hướng hot của thời trang hiện tại. Thông thường mình đều đánh giá từ uppers rồi mới đến phần đế. Nhưng phiên bản này đặc biệt hết sức, phải đi ngược lại thôi. Đôi giày vừa cầm trên tay đã thấy vô cùng chắc chắn và có vẻ nặng. Mình sợ mang vào sẽ hơi khó chịu ở chân nhưng hoàn toàn ngược lại, nó fit với chân mình hoàn hảo và tung tăng thì vô tư.</p>
                                </div>
                                <h3>
                                    <label for="tab2" id="lt2">Thông số <i class="fa fa-chevron-down" aria-hidden="true"></i></label>
                                </h3>
                                <div id="content2" class="section">
                                    <table cellpadding="10" id="t01" style="border-collapse:collapse;padding:5px;width:50%;" width="10">
                                        <tbody>
                                        <tr style="background-color: rgb(238, 238, 238);">
                                            <td  style="font-size: 12px; color: rgb(0, 0, 0); border-collapse: collapse; padding: 20px 0;" width="206">
                                                <strong>&nbsp; &nbsp;Thương hiệu</strong>
                                            </td>
                                            <td style="font-size: 12px; color: rgb(0, 0, 0); border-collapse: collapse; padding: 20px 0;" width="466">
                                                Adidas
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 12px; color: rgb(0, 0, 0); border-collapse: collapse; padding: 20px 0;">
                                                <strong>&nbsp; &nbsp;Mẫu</strong></td>
                                            <td style="font-size: 12px; color: rgb(0, 0, 0); border-collapse: collapse; padding: 20px 0;">
                                                Classic 2019
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <p style="color: rgb(0, 0, 0); font-size: 12px; line-height: normal;">
                                        <br/>
                                        <strong>Giày không phù hợp ngâm trong nước</strong>
                                        <br/>
                                        &nbsp;</p>
                                </div>
                                <h3>
                                    <label for="tab3" id="lt3">Thanh toán và vận chuyển <i class="fa fa-chevron-down" aria-hidden="true"></i></label>
                                </h3>
                                <div id="content3" class="section">
                                    <p>
                                        <span style="font-size:16px;"><strong><span>Chính sách thanh toán</span></strong></span>
                                    </p>
                                    <ul>
                                        <li>
                                            <span style="font-size:12px;">Thanh toán qua PayPal hoặc Chuyển khoản ngân hàng
                                            </span>
                                        </li>
                                        <li>
                                            <span style="font-size:12px;">Thanh toán ngay lập tức sau khi mua</span>
                                        </li>
                                        <li>
                                            <span style="font-size:12px;">Sản phẩm sẽ được gửi cho bạn trong vòng 1-7 ngày tùy phương thức thanh toán<em>(không bao gồm cuối tuần và ngày lễ)</em></span>
                                            <img alt="" src="/images/Payment.png"/>
                                        </li>
                                    </ul>
                                    <hr/>
                                    <p>
                                        <strong><span style="font-size:16px;"><span>Chính sách giao hàng</span></span></strong>
                                    </p>
                                    <ul>
                                        <li>
                                            <span style="font-size:12px;"><span>Phạm vi toàn lãnh thổ Việt Nam</span></span>
                                        </li>
                                        <li>
                                            <span style="font-size:12px;"><span>Hàng sẽ được vận chuyển bởi: <span
                                                        style="color:#FF0000;">Viettel Post hoặc Giao hàng tiết kiệm</span></span></span>
                                        </li>
                                        <li>
                                            <span style="font-size:12px;"><span>Thời gian giao hàng: <strong><em>(Thời gian dự tính, không bao gồm cuối tuần và ngày lễ)​</em></strong></span></span>
                                        </li>
                                        <li>

                                            <strong><span style="background-color:#D3D3D3;">Giao hàng tiêu chuẩn</span></strong>&nbsp;1-5 ngày làm việc<br/>
                                            <em>Vùng sâu vùng sa có thể mất tới hơn 7 ngày làm việc.</em><br/>
                                            <br/>
                                            <br/>
                                            <strong><span
                                                    style="background-color:#FFFF00;">Giao hàng nhanh</span></strong>1-3 ngày làm việc&nbsp;<br/>
                                            <em>Vùng sâu vùng sa có thể mất tới hơn 5 ngày làm việc.</em>
                                        </li>
                                    </ul>
                                </div>
                                <h3>
                                    <label for="tab4" id="lt4">Trả hàng và bảo hành
                                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                    </label>
                                </h3>
                                <div id="content4" class="section">
                                    <p>
                                        <span style="font-size:16px;"><strong><span>Chính sách hoàn trả</span></strong>
                                        </span>
                                    </p>
                                    <p>
                                        <span style="font-size:12px;"><span><strong>Hoàn tiền cho mặt hàng chỉ được chấp nhận khi:</strong></span></span>
                                    </p>
                                    <ul>
                                        <li>
                                            <span style="font-size:12px;"><span>Nếu người mua nhận được các mặt hàng bị hư hỏng hoặc trong tình trạng lỗi</span></span>
                                        </li>
                                        <li>
                                            <span style="font-size:12px;"><span>Tất cả các mã đăng ký, thẻ quà tặng và mã phiếu giảm giá sẽ không được hoàn lại</span></span>
                                        </li>
                                        <li>
                                            <span style="font-size:12px;"><span>Người mua chịu trách nhiệm cho tất cả các chi phí trả lại, vận chuyển và xử lý. Bạn có thể chọn phương thức riếng cảu bạn một cách hợp lý.</span></span>
                                        </li>
                                        <li>
                                            <span style="font-size:12px;"><span>Khi trả lại sản phẩm bị hư hỏng hoặc bị lỗi, vui lòng đảm bảo bạn đã bao gồm tất cả các mặt hàng đi kèm khi mua hàng.</span></span>
                                        </li>
                                        <li>
                                            <span style="font-size:12px;"><span>Tất cả các phản hồi phải được thực hiện trong vòng 7 ngày, kể từ ngày nhận được hàng.</span></span>
                                        </li>
                                        <li>
                                            <span style="font-size:12px;"><span>Nếu bạn nhận được hàng bị hư hỏng hoặc lỗi thì vui lòng liên hệ với chúng tôi trước khi trả lại.</span></span>
                                        </li>
                                    </ul>
                                    <hr/>
                                    <p>
                                        <strong><span style="font-size:16px;">Điều khoản</span></span></strong><br/>
                                    </p>
                                    <p>
                                        <span style="font-size:12px;"><span><strong>&nbsp;</strong></span></span><strong>Trường hợp ngoại lệ:</strong>
                                    </p>
                                    <ul>
                                        <li>Chúng tôi không hoàn lại tiền nếu:</li>
                                        <li>Bạn đã làm hỏng sản phẩm bằng cách sử dụng nó theo cách không được sử dụng.</li>
                                    </ul>
                                    <hr/>
                                    <p>
                                        <span style="font-size:16px;"><span><strong>Chính sách bảo hành</strong></span></span><br/>
                                    </p>
                                    <ul>
                                        <li>
                                            <span style="font-size:12px;"><span>Tất cả các sản phẩm được bảo hành tối thiểu 12 tháng.</span></span><br/>
                                        </li>
                                        <li>
                                            <span style="font-size:12px;"><span>Vui lòng xem thông tin bảo hành của nhà sản xuất hoặc nhắn tin cho chúng tôi để biết thời gian bảo hành cụ thể của từng sản phẩm.</span></span>
                                        </li>
                                        <li>
                                            <span style="font-size:12px;"><span><em>Bảo hành bắt đầu từ ngày thanh toán được thực hiện.</em></span></span>
                                        </li>
                                    </ul>
                                </div>
                                <h3><label for="tab7" id="lt7">Review <i class="fa fa-chevron-down"
                                                                         aria-hidden="true"></i></label></h3>
                                <div id="content7" class="section">
                                    <div class="tab-pane" id="reviews">
                                        <h4>Hãy là người đầu tiên nhận xét sản phẩm này!</h4>
                                        <p>Giúp người dùng S-shoe khác mua sắm thông minh hơn bằng cách viết đánh giá cho các sản phẩm bạn đã mua.</p>
                                        <p>
                                            <a href="#" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> Viết một nhận xét</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="custompageright">
                                <div class="morefromright">Sản phẩm cùng loại</div>
                                <div class="rightproductsinpage">
                                    <div class="row">
                                        @foreach($productRelationships as $key=>$value)
                                            <div class="wrapper-thumbnail col-xs-8 col-sm-6 col-md-4 col-lg-3">
                                                <input id="check-sku" type="hidden" value="{{$value->code}}">
                                                <div class="thumbnail">
                                                    <div class="pimage">
                                                        <a href="#" title="{{$value->full_name}}">
                                                            <img class="lazy" src="/images/{{$value->images}}" class="product-image" alt="{{$value->full_name}}"/>
                                                        </a>
                                                    </div>
                                                    <div class="caption">
                                                        <h3 itemprop="name">
                                                            <a href="#" title="{{$value->full_name}}">{{$value->full_name}}</a>
                                                        </h3>
                                                        @if($value->allow_sale == 1)
                                                            @if($value->discount_type == 1)
                                                                <p class="price">
                                                                    <span itemprop="price">{{number_format($value->price_base - ($value->price_base)*($value->discount)/100)}} VND</span>
                                                                    <span class="label label-default rrpstyle">{{number_format($value->price_base)}} VND</span>
                                                                </p>
                                                            @elseif($value->discount_type == 2)
                                                                <p class="price">
                                                                    <span itemprop="price">{{number_format($value->price_base - $value->discount)}} VND</span>
                                                                    <span class="label label-default rrpstyle">{{number_format($value->price_base)}} VND</span>
                                                                </p>
                                                            @endif
                                                        @else
                                                            <p class="price">
                                                                <span itemprop="price">{{number_format($value->price_base)}} VND</span>
                                                            </p>
                                                        @endif
                                                    </div>
                                                    <div class="savings-container">
                                                    </div>
                                                    <form class="form-inline buying-options">
                                                        <input type="hidden" id="sku{{$value->code}}"
                                                               name="sku" value="{{$value->code}}">
                                                        <input type="hidden" id="model{{$value->code}}"
                                                               name="model" value="{{$value->code}}">
                                                        <input type="hidden" id="thumb{{$value->images}}"
                                                               name="thumb" value="/images/{{$productDetail->images}}">
                                                        <a href="#"
                                                           title="Tùy chọn"
                                                           class="btn btn-primary btn-block btn-loads"
                                                           data-loading-text="<i class='fa fa-spinner fa-spin' style='font-size: 14px'></i>">Tùy chọn</a>
                                                    </form>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="removeaddtocart">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('block.testimonial')

</div>

</div>
</div>

</div>

@include('block.footer')

<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jcarousel.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<script type="text/javascript" src="/js/netoTicker.js"></script>
<script type="text/javascript" src="/js/jquery.lazy.min.js"></script>
<script type="text/javascript" src="/js/h_customer.js"></script>

<script type="text/javascript" language="JavaScript">
    $(window).scroll(function () {
        if ($(this).scrollTop() > 250) {
            $('.go-top').fadeIn();
        } else {
            $('.go-top').fadeOut();
        }
    });

    $(".go-top").on("click", function () {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0});
    });

    setInterval(function(){
        if($("#recent_sales_ticker").hasClass("fadeOutLeftd")){
            console.log('11112');
            $("#recent_sales_ticker").removeClass("fadeOutLeftd");
        } else {
            console.log('11188');
            $("#recent_sales_ticker").addClass("fadeOutLeftd");
        }
    }, 5000);
</script>

<script type="text/javascript" src="/js/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="//cdn.neto.com.au/assets/neto-cdn/jcountdown/1.4/jquery.jcountdown.min.js"></script>
<script type="text/javascript" src="//cdn.neto.com.au/assets/neto-cdn/zoom/1.4/jquery.zoom-min.js"></script>
<script type='text/javascript' src='/js/pop_up_script_neto_tag_live.js'></script>

<div id="recent_sales_ticker" class="animated fadeInLeft">
    <div id="recent_sales_close" class="btn pull-right">×</div>
    <div id="recent_sales_content">
        <table>
            <tbody>
            <tr>
                <td id="recent_sales_content_img">
                    <a href="/sandisk-64-gb-64g-cruzer-fit-usb-2.0-flash-drive-n"><img src="/assets/thumb/RVM4034.jpg?1531656284"></a>
                </td>
                <td id="recent_sales_content_description">Someone in <span style="text-transform: capitalize">seaton</span> bought a
                    <br><a href="/sandisk-64-gb-64g-cruzer-fit-usb-2.0-flash-drive-n">SanDisk 64 GB 64G Cruzer Fit USB 2.0 Flash Drive Nano Stick 64GB SDCZ33</a> <span class="ago">15 hours ago</span></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="nactivity" style="position: absolute; top: 533px; left: 924px; display: none; width: auto;" nactivity-status="ready" overlay-level="">
    <img src="{{asset("/images/loading.gif")}}" alt="Loading...">
</div>
<div class="noverlay" style="display: none; z-index: 99997; left: 0px; top: 0px; opacity: 0.5; position: absolute; background-color: rgb(0, 0, 0); height: 4707px; width: auto;"></div>
<div class="npopup" npopup-id="" style="position: absolute;/* display: block; */top: 205px;left: 674px;width: 500px;" npopup-status="active" npopup-overlay="false" npopup-width="" npopup-min-width="0" npopup-max-width="0" overlay-level="">
    <div class="npopup-body">
        <div class="successaddmessage">
            <div class="header modal-header">Items have been added to your cart
                <a href="javascript:void(0);" class="npopup-btn-close"></a>
            </div>
            <div class="body modal-body">
                <div class="description">All selected items added to cart.
                    <br>Use the buttons below to continue.</div>
                <div class="successaddmessageclear"></div>
            </div>
            <div class="footer">
                <div class="left">
                    <button type="button" class="btn btn-default npopup-continue" onclick="$.nClosePopupBox()" title="Continue Shopping">Continue Shopping</button>
                    <button type="button" class="btn btn-default npopup-view" onclick="window.location='https://www.ravsin.com.au/_mycart?ts=1568822266006515';" title="View Cart">View My Cart</button>
                </div>
                <div class="right">
                    <button type="button" onclick="window.location='https://www.ravsin.com.au/_mycart?ts=1568822266006515&amp;fn=payment';" title="Checkout" class="calltoaction btn btn-success npopup-checkout">Checkout Now</button>
                </div>
            </div>
            <div class="successaddmessageclear"></div>
        </div>
    </div>
</div>

</body>
</html>
