<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Giày Việt xuất khẩu, giá hấp dẫn, chất lượng cao"/>
    <meta name="description" content="Giày Việt xuất khẩu, giá hấp dẫn, chất lượng cao"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0"/>
    <meta property="og:title" content="{{$title}}"/>
    <meta property="og:site_name" content="unitymta.com"/>

    <title>{{$title}}</title>

    <link rel="canonical" href="/"/>
    <link rel="shortcut icon" href="/images/icon/favicon_logo.png"/>
    <link class="theme-selector" rel="stylesheet" type="text/css" href="/css/app.css" media="all"/>
    <link class="theme-selector" rel="stylesheet" type="text/css" href="/css/customcss.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="/css/style.css" media="all"/>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,300' rel='stylesheet' type='text/css'>
    <link class="theme-selector" rel="stylesheet" type="text/css" href="/css/mobileresponsive.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="/css/netoTicker.css" media="all"/>
    <link rel="stylesheet" type="text/css"
          href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui-1.8.18.custom.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="/css/jquery.fancybox.css" media="all"/>

    <!--[if lte IE 8]>
    <script type="text/javascript" src="/js/html5shiv.js"></script>
    <script type="text/javascript" src="/js/respond.min.js"></script>
    <![endif]-->

</head>
<body>

@include('block.header')

<div id="main-content" class="container">

    @yield('content')

</div>

</div>
</div>

</div>

@include('block.footer')

<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jcarousel.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<script type="text/javascript" src="/js/netoTicker.js"></script>
<script type="text/javascript" src="/js/jquery.lazy.min.js"></script>
<script type="text/javascript" src="/js/h_customer.js"></script>

<script type="text/javascript" language="JavaScript">
    $(window).scroll(function () {
        if ($(this).scrollTop() > 250) {
            $('.go-top').fadeIn();
        } else {
            $('.go-top').fadeOut();
        }
    });

    $(".go-top").on("click", function () {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0});
    });
</script>

<script type="text/javascript" src="/js/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="//cdn.neto.com.au/assets/neto-cdn/jcountdown/1.4/jquery.jcountdown.min.js"></script>
<script type="text/javascript" src="//cdn.neto.com.au/assets/neto-cdn/zoom/1.4/jquery.zoom-min.js"></script>
<script type='text/javascript' src='/js/pop_up_script_neto_tag_live.js'></script>

</body>
</html>
