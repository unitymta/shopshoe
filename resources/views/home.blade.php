<!DOCTYPE html>
<html lang="vn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Home"/>
    <meta name="description" content="Giày Việt xuất khẩu, giá hấp dẫn, chất lượng cao"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0"/>
    <meta property="og:title" content="Giày Việt xuất khẩu"/>
    <meta property="og:site_name" content="unitymta.com"/>

    <title>Giày Việt xuất khẩu</title>

    <link rel="canonical" href="/"/>
    <link rel="shortcut icon" href="/images/icon/favicon_logo.png"/>
    <link class="theme-selector" rel="stylesheet" type="text/css" href="/css/app.css" media="all"/>
    <link class="theme-selector" rel="stylesheet" type="text/css" href="/css/customcss.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="/css/style.css" media="all"/>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,300' rel='stylesheet' type='text/css'>
    <link class="theme-selector" rel="stylesheet" type="text/css" href="/css/mobileresponsive.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="/css/netoTicker.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui-1.8.18.custom.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="/css/jquery.fancybox.css" media="all"/>

    <!--[if lte IE 8]>
    <script type="text/javascript" src="/js/html5shiv.js"></script>
    <script type="text/javascript" src="/js/respond.min.js"></script>
    <![endif]-->

</head>
<body id="n_home">

@include('block.header')

<div id="main-content" class="container">
    <div class="row"></div>
</div>
<div class="container">
    <div class="slideshowhomepage">
        <div class="wrapper-carousel">
            <div id="homepageCarousel" class="carousel slide">
                <ol class="carousel-indicators">
                    <li data-target="#homepageCarousel" data-slide-to="0" class="active"></li>
                </ol>
                <div class="carousel-inner">
                    <div id="0" class="item active">
                        <a href="#" target="">
                            <img src="/images/banner.jpg"
                                 class="home-banner-item" alt="s-shoe" border="0">
                        </a>
                    </div>
                    <div id="0" class="item">
                        <a href="#" target="">
                            <img src="/images/banner2.jpg"
                                 class="home-banner-item" alt="s-shoe" border="0">
                        </a>
                    </div>
                </div>
                <a class="left carousel-control" href="#homepageCarousel" data-slide="prev">
                    <span class="fa fa-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#homepageCarousel" data-slide="next">
                    <span class="fa fa-chevron-right"></span>
                </a>
            </div>
        </div>
    </div>
    <div class="hotdealsright">
        <h2>Ưu đãi</h2>
        <div class="hotdealproduct">
            <div class="row2">
                <div class="wrapper-thumbnail col-xs-8 col-sm-6 col-md-4 col-lg-3">
                    <input id="check-sku" type="hidden" value="">
                    <div class="thumbnail">
                        <div class="pimage">
                            <a href="#" title="">
                                <img src="" class="product-image" alt=""/>
                            </a>
                        </div>
                        <div class="caption">
                            <h3 itemprop="name">
                                <a href="#" title="">demo product</a>
                            </h3>
                            <p class="price">
                                <span itemprop="price">10000 VND</span>
                            </p>
                        </div>
                        <div class="savings-container">
                        </div>
                        <form class="form-inline buying-options">
                            <input type="hidden" id="skujwNA5RVA2295-2300" name="skujwNA5RVA2295-2300" value="RVA2295-2300">
                            <input type="hidden" id="modeljwNA5RVA2295-2300" name="modeljwNA5RVA2295-2300" value="">
                            <input type="hidden" id="thumbjwNA5RVA2295-2300" name="thumbjwNA5RVA2295-2300" value="/assets/thumb/">
                            <a href="#" title="Buying Options" class="btn btn-primary btn-block btn-loads"
                               data-loading-text="<i class='fa fa-spinner fa-spin' style='font-size: 14px'></i>">Hiển thị tùy chọn</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">

            @include('block.assurance')

            <div class="bgpage">
                <div class="mainhomepage">
                    <div class="faturedcategories">
                        <div class="categoryimg">
                            <a href="#" title="">
                                <img alt="" class="lazy" src="/images/combo-1.jpg"/>
                            </a>
                        </div>
                        <div class="categoryimg">
                            <a href="#" title="">
                                <img alt="" class="lazy" src="/images/combo-2.jpg"/>
                            </a>
                        </div>
                        <div class="categoryimg">
                            <a href="#" title="">
                                <img alt="" class="lazy" src="/images/combo-1.jpg"/>
                            </a>
                        </div>
                    </div>
                    <div class="faturedcategories">
                        <div class="categoryimg">
                            <a href="#" title="">
                                <img alt="" class="lazy" src="/images/combo-2.jpg"/>
                            </a>
                        </div>
                        <div class="categoryimg">
                            <a href="#" title="">
                                <img alt="" class="lazy" src="/images/combo-1.jpg"/>
                            </a>
                        </div>
                        <div class="categoryimg">
                            <a href="#" title="">
                                <img alt="" class="lazy" src="/images/combo-2.jpg"/>
                            </a>
                        </div>
                    </div>

                    <div class="newproducts">
                        <div class="newproductsheader">hàng mới</div>
                        <div class="tableproducts">
                            <input id="tab1" type="radio" name="tabs" checked>
                            <label for="tab1"></label>
                            <section id="content1">
                                <div class="row2">
                                    <?php
                                        foreach ($productNew as $key => $value){ ?>
                                            <div class="wrapper-thumbnail col-xs-8 col-sm-6 col-md-4 col-lg-3">
                                                <input id="check-sku" type="hidden" value="">
                                                <div class="thumbnail">
                                                    <div class="pimage">
                                                        <a href="{{route('detail', ['slug'=>$value->slug_text, 'product_id' => $value->product_id])}}" title="{{$value->name}}">
                                                            <img class="product-image" src="/images/{{$value->images}}" alt="{{$value->name}}"/>
                                                        </a>
                                                    </div>
                                                    <div class="caption">
                                                        <h3 itemprop="name">
                                                            <a href="{{route('detail', ['slug'=>$value->slug_text, 'product_id' => $value->product_id])}}" title="{{$value->name}}">{{$value->full_name}}</a>
                                                        </h3>
                                                        <p class="price">
                                                            @if($value->allow_sale == 1)
                                                                @if($value->discount_type == 1)
                                                                    <span>{{number_format($value->price_base - ($value->price_base)*($value->discount)/100)}} VND</span>
                                                                @elseif($value->discount_type == 2)
                                                                    <span>{{number_format($value->price_base - $value->discount)}} VND</span>
                                                                @endif
                                                                <span class="label label-default">Giá cũ {{number_format($value->price_base)}} VND</span>
                                                            @else
                                                                <span>{{number_format($value->price_base)}} VND</span>
                                                            @endif
                                                        </p>
                                                    </div>
                                                    @if($value->allow_sale == 1)
                                                        @if($value->discount_type == 1)
                                                            <div class="savings-container">
                                                                <span class="label productsavex">Giảm {{number_format(($value->price_base)*($value->discount)/100)}} VND</span>
                                                                <span class="label label-warning percentbutton">(-{{$value->discount}}%)</span>
                                                            </div>
                                                        @elseif($value->discount_type == 2)
                                                            <div class="savings-container">
                                                                <span class="label productsavex">Giảm {{number_format($value->discount)}}VND</span>
                                                            </div>
                                                        @endif
                                                    @endif
                                                    <form class="form-inline buying-options">
                                                        <input type="hidden" id="skuBSMGi" name="skuBSMGi" value="">
                                                        <input type="hidden" id="modelBSMGi" name="modelBSMGi" value="{{$value->name}}">
                                                        <input type="hidden" id="thumbBSMGi" name="thumbBSMGi" value="/images/thumb/">
                                                        <input type="text" id="qtyBSMGi" name="qtyBSMGi" value="" placeholder="Qty" class="input-tiny">
                                                        <button type="button" title="Thêm vào giỏ" class="addtocart btn-primary btn">
                                                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        <?php }?>
                                </div>
                                <div class="viewmore"><a href="#" title="Xem thêm">Xem thêm</a></div>
                            </section>
                        </div>
                    </div>
                    <div class="newproducts">
                        <div class="newproductsheader">bán chạy nhất</div>
                        <div class="tablesleproducts">
                            <input id="tables1" type="radio" name="tabless" checked>
                            <label for="tables1"></label>
                            <section id="content1">
                                <div class="row2">
                                    <?php foreach ($productTopSeller as $key => $value){ ?>
                                        <div class="wrapper-thumbnail col-xs-8 col-sm-6 col-md-4 col-lg-3">
                                            <input id="check-sku" type="hidden" value="">
                                            <div class="thumbnail">
                                                <div class="pimage">
                                                    <a href="{{route('detail', ['slug'=>$value->slug_text, 'product_id' => $value->product_id])}}" title="{{$value->name}}">
                                                        <img class="product-image" src="/images/{{$value->images}}" alt="{{$value->name}}"/>
                                                    </a>
                                                </div>
                                                <div class="caption">
                                                    <h3 itemprop="name">
                                                        <a href="{{route('detail', ['slug'=>$value->slug_text, 'product_id' => $value->product_id])}}" title="{{$value->name}}">{{$value->full_name}}</a>
                                                    </h3>
                                                    <p class="price">
                                                        @if($value->allow_sale == 1)
                                                            @if($value->discount_type == 1)
                                                                <span>{{number_format($value->price_base - ($value->price_base)*($value->discount)/100)}} VND</span>
                                                            @elseif($value->discount_type == 2)
                                                                <span>{{number_format($value->price_base - $value->discount)}} VND</span>
                                                            @endif
                                                            <span class="label label-default">Giá cũ {{number_format($value->price_base)}} VND</span>
                                                        @else
                                                            <span>{{number_format($value->price_base)}} VND</span>
                                                        @endif
                                                    </p>
                                                </div>
                                                @if($value->allow_sale == 1)
                                                    @if($value->discount_type == 1)
                                                        <div class="savings-container">
                                                            <span class="label productsavex">Giảm {{number_format(($value->price_base)*($value->discount)/100)}} VND</span>
                                                            <span class="label label-warning percentbutton">(-{{$value->discount}}%)</span>
                                                        </div>
                                                    @elseif($value->discount_type == 2)
                                                        <div class="savings-container">
                                                            <span class="label productsavex">Giảm {{number_format($value->discount)}} VND</span>
                                                        </div>
                                                    @endif
                                                @endif
                                                <form class="form-inline buying-options">
                                                    <input type="hidden" id="skuFXWuaRVA2823" name="skuFXWuaRVA2823" value="RVA2823">
                                                    <input type="hidden" id="modelFXWuaRVA2823" name="modelFXWuaRVA2823" value="{{$value->name}}">
                                                    <input type="hidden" id="thumbFXWuaRVA2823" name="thumbFXWuaRVA2823" value="/assets/thumb/">
                                                    <input type="text" id="qtyFXWuaRVA2823" name="qtyFXWuaRVA2823" value="" placeholder="Qty" class="input-tiny">
                                                    <button type="button" title="Thêm vào giỏ" class="addtocart btn-primary btn" rel="FXWuaRVA2823">
                                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="viewmore"><a href="#" title="Xem thêm">Xem thêm</a></div>
                            </section>
                        </div>
                    </div>
                    <div class="newproducts">
                        <div class="featuredproducheader">nổi bật</div>
                        <div class="featuredproducthere">
                            <div class="row2">
                                <?php
                                    foreach ($productFeature as $key => $value){ ?>
                                        <div class="wrapper-thumbnail col-xs-8 col-sm-6 col-md-4 col-lg-3">
                                            <div class="thumbnail">
                                                <div class="pimage">
                                                    <a href="{{route('detail', ['slug'=>$value->slug_text, 'product_id' => $value->product_id])}}" title="{{$value->name}}">
                                                        <img class="product-image" src="/images/{{$value->images}}" alt="{{$value->name}}"/>
                                                    </a>
                                                </div>
                                                <div class="caption">
                                                    <h3 itemprop="name">
                                                        <a href="{{route('detail', ['slug'=>$value->slug_text, 'product_id' => $value->product_id])}}" title="{{$value->name}}">{{$value->full_name}}</a>
                                                    </h3>
                                                    <p class="price">
                                                        @if($value->allow_sale == 1)
                                                            @if($value->discount_type == 1)
                                                                <span>{{number_format($value->price_base - ($value->price_base)*($value->discount)/100)}} VND</span>
                                                            @elseif($value->discount_type == 2)
                                                                <span>{{number_format($value->price_base - $value->discount)}} VND</span>
                                                            @endif
                                                        @else
                                                            <span>{{number_format($value->price_base)}} VND</span>
                                                        @endif
                                                    </p>
                                                </div>
                                                @if($value->allow_sale == 1)
                                                    @if($value->discount_type == 1)
                                                        <div class="savings-container">
                                                            <span class="label label-default rrpstyle">{{number_format($value->price_base)}} VND</span>
                                                            <span class="label productsavex">Giảm {{number_format((($value->price_base)*($value->discount)/100),0)}} VND</span>
                                                        </div>
                                                    @elseif($value->discount_type == 2)
                                                        <div class="savings-container">
                                                            <span class="label label-default rrpstyle">{{number_format($value->price_base)}} VND</span>
                                                            <span class="label productsavex">Giảm {{number_format($value->discount)}} VND</span>
                                                        </div>
                                                    @endif
                                                @endif
                                                <form class="form-inline buying-options">
                                                    <input type="hidden" id="sku5eGly" name="sku5eGly" value="">
                                                    <input type="hidden" id="model5eGly" name="model5eGly" value="{{$value->name}}">
                                                    <input type="hidden" id="thumb5eGly" name="thumb5eGly" value="/assets/thumb/">
                                                    <input type="text" id="qty5eGly" name="qty5eGly" value="" placeholder="Qty" class="input-tiny">
                                                    <button type="button" title="Thêm vào giỏ" class="addtocart btn-primary btn">
                                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                <?php }?>
                            </div>
                            <div class="viewmore"><a href="#" title="Xem thêm">Xem thêm</a></div>
                        </div>
                    </div>

                    @include('block.testimonial')
                </div>
            </div>
        </div>
        <style>
            body {
                background-color: #F5F5F5;
            }
        </style>
    </div>
</div>

@include('block.footer')

<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jcarousel.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<script type="text/javascript" src="/js/netoTicker.js"></script>
<script type="text/javascript" src="/js/jquery.lazy.min.js"></script>
<script type="text/javascript" src="/js/h_customer.js"></script>

<script type="text/javascript" language="JavaScript">
    $(window).scroll(function () {
        if ($(this).scrollTop() > 250) {
            $('.go-top').fadeIn();
        } else {
            $('.go-top').fadeOut();
        }
    });

    $(".go-top").on("click", function () {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0});
    });
</script>
<script type="text/javascript" src="/js/jquery-ui-1.8.18.custom.min.js"></script>
<script type='text/javascript' src='/js/pop_up_script_neto_tag_live.js'></script>

</body>
</html>
