@if (Route::currentRouteName() == 'detail')
    <div class="wrapper-assurance top-cate-assu hidden-lg hidden-md">
@else
   <div class="wrapper-assurance">
@endif
    <div class="row">
        <div class="col-xs-6 col-sm-2">
            <div class="thumbnail-assurance">
                <div class="image">
                    <img src="/images/icon/35.png" alt="Chất lượng và Chính hãng" border="0">
                </div>
                <div class="caption-assurance">
                    <h5 class="headline">Chất lượng</h5>
                    <p>Đảm bảo tuyệt đối</p>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-2">
            <div class="thumbnail-assurance">
                <div class="image">
                    <img src="/images/icon/43.png" alt="Đổi trả 7 ngày" border="0">
                </div>
                <div class="caption-assurance">
                    <h5 class="headline">Đổi trả</h5>
                    <p>Miễn phí trong 7 ngày</p>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-2">
            <div class="thumbnail-assurance">
                <div class="image">
                    <img src="/images/icon/44.png" alt="Chuyển phát nhanh" border="0">
                </div>
                <div class="caption-assurance">
                    <h5 class="headline">Chuyển phát</h5>
                    <p>Nhanh, tiết kiệm</p>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-2">
            <div class="thumbnail-assurance">
                <div class="image">
                    <img src="/images/icon/45.png" alt="Việt Nam" border="0">
                </div>
                <div class="caption-assurance">
                    <h5 class="headline">Phạm vi</h5>
                    <p>Toàn Việt Nam</p>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-2">
            <div class="thumbnail-assurance">
                <div class="image">
                    <img src="/images/icon/46.png" alt="Hậu mãi" border="0">
                </div>
                <div class="caption-assurance">
                    <h5 class="headline">Hậu mãi</h5>
                    <p>Tận tình, chu đáo</p>
                </div>
            </div>
        </div>
    </div>
</div>
