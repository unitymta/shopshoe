<div class="brandsbottomwide">
    <div class="brandsbottom">
        <div class="brandimg"><img src="/images/brands/adidas-logo.png" alt="Adidas"></div>
        <div class="brandimg"><img src="/images/brands/converse-logo.png" alt="Converse"></div>
        <div class="brandimg"><img src="/images/brands/nike-logo.png" alt="nike"></div>
    </div>
</div>
<div class="wrapper-footer">
    <div class="main-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 colcustom1">
                            <div class="aboutusfooter"><h2>Giới thiệu</h2>
                                <p><span>S-shoe là cửa hàng giày Việt xuất khẩu. Cung cấp đa dạng mẫu giày, giá tốt, phục vụ tận tình, chế độ hậu mãi, bảo hành tuyệt vời.</span></p>
                            </div>
                            <div class="tradinghours"><h2>Giờ mở cửa</H2>
                                <p>Tất cả các ngày trong tuần (kể cả ngày lễ)</p>
                            </div>
                            <div class="footerinfo">
                                <p>
                                    <i class="fa fa-map-marker" aria-hidden="true"></i> Thọ Xuân - Đan Phượng - Hà Nội
                                </p>
                                <p>
                                    <i class="fa fa-envelope" aria-hidden="true"></i> tranducsang.mta@gmail.com
                                </p>
                            </div>
                        </div>
                        <div class="footercentermenu">
                            <div class="col-xs-12 col-sm-4 colcustom3">
                                <h4>Về S-shoe</h4>
                                <ul class="nav nav-pills nav-stacked">
                                    <li><a href="/">Giới thiệu S-shoe</a></li>
                                    <li><a href="/">Liên hệ</a></li>
                                    <li><a href="/">Blog S-shoe</a></li>
                                    <li><a href="/">Hỏi đáp</a></li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-4 colcustom3 after">
                                <h4>Trợ giúp</h4>
                                <ul class="nav nav-pills nav-stacked">
                                    <li><a href="/page/payment-policy/">Chính sách thanh toán</a></li>
                                    <li><a href="/page/delivery-information/">Chính sách vận chuyển</a></li>
                                    <li><a href="/page/returns-policy/">Chính sách đổi trả</a></li>
                                    <li><a href="/page/warranty-policy/">Chính sách bảo hành</a></li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-4 colcustom3 before">
                                <h4 style="display: none">Click to Edit Name</h4>
                                <ul class="nav nav-pills nav-stacked">
                                    <li><a href="/page/information-pages/terms-conditions/">Điều khoản sử dụng</a></li>
                                    <li><a href="/privacy_policy">Chính sách riêng tư</a></li>
                                    <li><a href="/security_policy">Chính sách bảo mật</a></li>
                                    <li><a href="/_myacct">Tài khoản</a></li>
                                    <li><a href="/_myacct#orders">Theo dõi đơn hàng</a></li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-4 colcustom3">
                                <h4>theo dõi chúng tôi</h4>
                                <ul class="list-inline list-social social-share">
                                    <li>
                                        <a href="https://www.facebook.com/sang.soanseo" target="_blank">
                                            <i class="fa fa-facebook" aria-hidden="true" style="color:#3b5998;"></i>
                                            Facebook
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://twitter.com/tranxuansang193" target="_blank">
                                            <i class="fa fa-twitter" aria-hidden="true" style="color:#00acee;"></i>
                                            Twitter
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/channel/UCNwCs9WIif10i_El9SkrkZQ"
                                           target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i>
                                            Youtube
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/unitymta/" target="_blank">
                                            <i class="fa fa-instagram" aria-hidden="true"></i>
                                            Instagram
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 colcustom4">
                            <h3>Đăng ký</h3>
                            <p>Để nhận thông tin mới nhất về các chương trình khuyến mãi</p>
                            <form method="post" action="#">
                                <input type="hidden" name="list_id" value="1">
                                <input type="hidden" name="opt_in" value="y">
                                <div class="input-group">
                                    <input name="inp-email" class="form-control" placeholder="Email..." type="email" value="" placeholder="cĐịa chỉ email" required/>
                                    <input name="inp-submit" type="hidden" value="y"/>
                                    <input name="inp-opt_in" type="hidden" value="y"/>
                                    <span class="input-group-btn"><input class="btn btn-default" type="submit" value="Đăng ký"/></span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="list-social-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="list-inline list-social">
                        <li>
                            <div class="payment-icon-container">
                                <div class="payment-icon payment-icon-mastercard"></div>
                            </div>
                        </li>
                        <li>
                            <div class="payment-icon-container">
                                <div class="payment-icon payment-icon-visa"></div>
                            </div>
                        </li>
                        <li>
                            <div class="payment-icon-container">
                                <div class="payment-icon payment-icon-paypal"></div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper-payment">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="adressfooter">
                    <ul class="address">
                        <li>&copy; 2019 S-SHOE | All rights reserved</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <a href="javascript:void(0)" class="go-top"><i class="fa fa-chevron-up"></i></a>
</div>
