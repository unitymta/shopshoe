<div class="wrapper-sticky">
    <div class="top-links">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
                </div>
                <div class="hidden-xs hidden-md hidden-sm col-lg-9 col-lg-9-custom">
                    <ul class="nav navbar-nav" id="top-navbar">
                        <div class="topmenucustom">
                            <div class="currencycustom">
                                <div class="nav-actcurrency">
                                    <div class="nav-currency btn-group">
                                        <button type="button" class="btn btn-default drop-down-toggle"
                                                data-toggle="dropdown">
                                            <img src="/images/icon/vn.png" alt="Viet Nam">
                                            <span>VN</span>
                                            <i class="fa fa-chevron-down"></i>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="#" class="flagimg">
                                                    <img src="/images/icon/vn.png" alt="Vietnam"><span>VN</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="flagimg">
                                                    <img src="/images/icon/en.png" alt="English"><span>EN</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <li class="login-register">
                            <a href="{{url('login')}}" title="Login">Đăng Nhập</a>
                            <span> / </span>
                            <a href="{{url('register')}}" title="Register">Đăng Ký</a>
                        </li>
                        <li class="lv1-li dropdown dropdown-hover myaccountcustom">
                            <a href="#">
                                <i class="fa fa-user"></i>
                                <span class="my-account">Tài Khoản</span>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="/login">Đăng ký / Đăng nhập</a></li>
                                <li><a href="#">Đơn đặt hàng</a></li>
                                <li><a href="#">Quotes</a></li>
                                <li><a href="#">Hóa đơn</a></li>
                                <li><a href="#">Yêu thích</a></li>
                                <li><a href="#">Wishlists</a></li>
                                <li><a href="#">Thiết lập thông tin</a></li>
                            </ul>
                        </li>
                        <div class="hidden-xs col-sm-4 nCustom-social-icons">
                            <ul class="list-inline list-social pull-right">
                                <li class="hidden">
                                    <a href="#" target="_blank"><i class="fa fa-envelope"></i></a>
                                </li>
                                <li>
                                    <a href="https://www.facebook.com/sang.soanseo" target="_blank"><i class="fa fa-facebook-square text-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/tranxuansang193" target="_blank"><i class="fa fa-twitter-square text-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="https://www.youtube.com/channel/UCNwCs9WIif10i_El9SkrkZQ" target="_blank"><i class="fa fa-youtube-square text-youtube"></i></a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/unitymta/" target="_blank"><i class="fa fa-instagram text-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="after-div"></div>
    <div id="menu-mobile" class="data-click">
        <div class="head">
            <h3>Categories</h3>
            <div class="topmenucustom">
                <div class="currencycustom">
                    <div class="nav-actcurrency">
                        <div class="nav-currency btn-group">
                            <button type="button" class="btn btn-default drop-down-toggle"
                                    data-toggle="dropdown">
                                <img src="/images/icon/vn.png" alt="Viet Nam">
                                <span>VN</span>
                                <i class="fa fa-chevron-down"></i>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="#" class="flagimg">
                                        <img src="/images/icon/vn.png" alt="Vietnam"><span>VN</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="flagimg">
                                        <img src="/images/icon/en.png" alt="English"><span>EN</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" title="account" class="acc-menu-mobile"><i class="fa fa-user"></i>Account</a>
            <span class="close"></span>
        </div>

        <ul class="nav navbar-nav">
            <li class="dropdownhassub">
                <a href="#" class="dropdown-toggle">Memory
                    Storage</a>
                <div class="after-div-lv2"></div>
                <ul class="dropdown-menu">
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="#"
                            class="nuhover dropdown-toggle">Memory Cards</a>
                        <ul class="sub-category">
                            <li class=""><a href="#">CF
                                    Cards</a></li>
                            <li class=""><a href="https://www.ravsin.com.au/microsd-cards/">microSD Cards</a></li>
                            <li class=""><a href="https://www.ravsin.com.au/sd-cards/">SD Cards</a></li>
                        </ul>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="https://www.ravsin.com.au/memory-storage/usb-flash-drives/"
                            class="nuhover dropdown-toggle">USB Flash Drives</a>
                        <ul class="sub-category">
                            <li class=""><a href="#">USB
                                    2.0</a></li>
                            <li class=""><a href="#">USB
                                    3.0</a></li>
                        </ul>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="https://www.ravsin.com.au/memory-storage/otg-drives/" class="nuhover dropdown-toggle">OTG
                            USB Drives</a>
                        <ul class="sub-category">
                            <li class=""><a
                                    href="https://www.ravsin.com.au/memory-storage/otg-usb-drives/android-micro-usb/">Android
                                    Micro USB</a></li>
                            <li class=""><a
                                    href="https://www.ravsin.com.au/memory-storage/otg-usb-drives/apple-lightning/">Apple
                                    Lightning</a></li>
                            <li class=""><a
                                    href="https://www.ravsin.com.au/memory-storage/otg-usb-drives/android-usb-c/">Android
                                    USB-C</a></li>
                        </ul>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="https://www.ravsin.com.au/memory-storage/card-readers/"
                            class="nuhover dropdown-toggle">Card Readers</a>
                        <ul class="sub-category">
                            <li class=""><a href="https://www.ravsin.com.au/memory-storage/card-readers/usb-2.0/">USB
                                    2.0</a></li>
                            <li class=""><a href="https://www.ravsin.com.au/memory-storage/card-readers/usb-3.0/">USB
                                    3.0</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="dropdownhassub">
                <a href="https://www.ravsin.com.au/phone-accessories/" class="dropdown-toggle">Phone
                    Accessories</a>
                <div class="after-div-lv2"></div>
                <ul class="dropdown-menu">
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="https://www.ravsin.com.au/phone-accessories/mobile-phone/"
                            class="nuhover dropdown-toggle">Mobile Phone</a>
                        <ul class="sub-category">
                            <li class=""><a
                                    href="https://www.ravsin.com.au/electronics/mobile-phones/screen-protectors/">Screen
                                    Protectors</a></li>
                            <li class=""><a href="https://www.ravsin.com.au/electronics/mobile-phones/cases-covers/">Cases
                                    & Covers</a></li>
                            <li class=""><a href="https://www.ravsin.com.au/electronics/mobile-phones/chargers/">Chargers</a>
                            </li>
                            <li class=""><a href="https://www.ravsin.com.au/electronics/mobile-phones/armbands/">Armbands</a>
                            </li>
                        </ul>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="https://www.ravsin.com.au/phone-accessories/tablet/" class="nuhover dropdown-toggle">Tablet</a>
                        <ul class="sub-category">
                            <li class=""><a href="https://www.ravsin.com.au/electronics/tablets/screen-protectors/">Screen
                                    Protectors</a></li>
                            <li class=""><a href="https://www.ravsin.com.au/electronics/tablets/cases-covers/">Cases &
                                    Covers</a></li>
                            <li class=""><a href="https://www.ravsin.com.au/electronics/tablets/chargers/">Chargers</a>
                            </li>
                        </ul>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="https://www.ravsin.com.au/phone-accessories/smart-watch/"
                            class="nuhover dropdown-toggle">Smart Watch</a>
                        <ul class="sub-category">
                            <li class=""><a href="https://www.ravsin.com.au/electronics/smart-watches/cases-covers/">Cases
                                    & Covers</a></li>
                            <li class=""><a href="https://www.ravsin.com.au/chargers/">Chargers</a></li>
                            <li class=""><a
                                    href="https://www.ravsin.com.au/electronics/smart-watches/screen-protectors/">Screen
                                    Protectors</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="dropdownhassub">
                <a href="https://www.ravsin.com.au/headphones-cables/" class="dropdown-toggle">Headphones
                    & Cables</a>
                <div class="after-div-lv2"></div>
                <ul class="dropdown-menu">
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="https://www.ravsin.com.au/headphones-cables/earphones/"
                            class="nuhover dropdown-toggle">Earphones</a>
                        <ul class="sub-category">
                            <li class=""><a href="https://www.ravsin.com.au/headphones-cables/earphones/wireless/">Wireless</a>
                            </li>
                            <li class=""><a
                                    href="https://www.ravsin.com.au/headphones-cables/earphones/wired/">Wired</a></li>
                        </ul>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="https://www.ravsin.com.au/headphones-cables/headphones/"
                            class="nuhover dropdown-toggle">Headphones</a>
                        <ul class="sub-category">
                            <li class=""><a href="https://www.ravsin.com.au/headphones-cables/headphones/wireless/">Wireless</a>
                            </li>
                            <li class=""><a
                                    href="https://www.ravsin.com.au/headphones-cables/headphones/wired/">Wired</a></li>
                        </ul>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category "><a
                            href="https://www.ravsin.com.au/audio-video/hdmi-cables/" class="nuhover dropdown-toggle">HDMI
                            Cables</a>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="https://www.ravsin.com.au/headphones-cables/headsets/"
                            class="nuhover dropdown-toggle">Headsets</a>
                        <ul class="sub-category">
                            <li class=""><a href="https://www.ravsin.com.au/headphones-cables/headsets/bluetooth/">Bluetooth</a>
                            </li>
                        </ul>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category "><a
                            href="https://www.ravsin.com.au/headphones-cables/audio-cables/"
                            class="nuhover dropdown-toggle">Audio Cables</a>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category "><a
                            href="https://www.ravsin.com.au/headphones-cables/adapters/"
                            class="nuhover dropdown-toggle">Adapters</a>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="https://www.ravsin.com.au/speakers/" class="nuhover dropdown-toggle">Speakers</a>
                        <ul class="sub-category">
                            <li class=""><a href="https://www.ravsin.com.au/headphones-cables/speakers/bluetooth/">Bluetooth</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="dropdownhassub">
                <a href="https://www.ravsin.com.au/tech.-gear-accessories/"
                   class="dropdown-toggle">Tech. Gear & Accessories</a>
                <div class="after-div-lv2"></div>
                <ul class="dropdown-menu">
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="https://www.ravsin.com.au/tech.-gear-more/mp3-player/"
                            class="nuhover dropdown-toggle">MP3 Player</a>
                        <ul class="sub-category">
                            <li class=""><a href="https://www.ravsin.com.au/electronics/mp3-players/screen-protectors/">Screen
                                    Protectors</a></li>
                            <li class=""><a href="https://www.ravsin.com.au/electronics/mp3-players/cases-covers/">Cases
                                    & Covers</a></li>
                            <li class=""><a
                                    href="https://www.ravsin.com.au/electronics/mp3-players/chargers/">Chargers</a></li>
                        </ul>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="https://www.ravsin.com.au/tech.-gear-more/action-camera/"
                            class="nuhover dropdown-toggle">Action Camera</a>
                        <ul class="sub-category">
                            <li class=""><a href="https://www.ravsin.com.au/electronics/gopro-hero/cameras/">Cameras</a>
                            </li>
                            <li class=""><a href="https://www.ravsin.com.au/electronics/gopro-hero/accessories/">Accessories</a>
                            </li>
                        </ul>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="https://www.ravsin.com.au/tech.-gear-accessories/printer-ink/"
                            class="nuhover dropdown-toggle">Printer Ink</a>
                        <ul class="sub-category">
                            <li class=""><a href="https://www.ravsin.com.au/tech.-gear-accessories/printer-ink/inkjet/">Inkjet</a>
                            </li>
                            <li class=""><a href="https://www.ravsin.com.au/tech.-gear-accessories/printer-ink/laser/">Laser</a>
                            </li>
                        </ul>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="https://www.ravsin.com.au/tech.-gear-more/digital-camera/"
                            class="nuhover dropdown-toggle">Digital Camera</a>
                        <ul class="sub-category">
                            <li class=""><a href="https://www.ravsin.com.au/electronics/digital-cameras/accessories/">Accessories</a>
                            </li>
                        </ul>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="https://www.ravsin.com.au/tech.-gear-more/receivers/" class="nuhover dropdown-toggle">Receivers</a>
                        <ul class="sub-category">
                            <li class=""><a
                                    href="https://www.ravsin.com.au/tech.-gear-accessories/receivers/media-players/">Media
                                    Players</a></li>
                            <li class=""><a href="https://www.ravsin.com.au/livetvhd">LIVE IPTV Players</a></li>
                            <li class=""><a href="https://www.ravsin.com.au/electronics/receivers/accessories/">Accessories</a>
                            </li>
                        </ul>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category "><a
                            href="https://www.ravsin.com.au/storage-stationary/labels/" class="nuhover dropdown-toggle">Labels</a>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category "><a
                            href="https://www.ravsin.com.au/tech.-gear-accessories/gadgets/"
                            class="nuhover dropdown-toggle">Gadgets</a>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="https://www.ravsin.com.au/tech.-gear-accessories/computer-peripherals/"
                            class="nuhover dropdown-toggle">Computer Peripherals</a>
                        <ul class="sub-category">
                            <li class=""><a href="https://www.ravsin.com.au/hubs-adapters/">Hubs & Adapters</a></li>
                            <li class=""><a
                                    href="https://www.ravsin.com.au/tech.-gear-accessories/computer-peripherals/mouse/">Mouse</a>
                            </li>
                            <li class=""><a
                                    href="https://www.ravsin.com.au/tech.-gear-accessories/computer-peripherals/webcam/">Webcam</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="dropdownhassub">
                <a href="https://www.ravsin.com.au/gaming-accessories/" class="dropdown-toggle">Gaming
                    Accessories</a>
                <div class="after-div-lv2"></div>
                <ul class="dropdown-menu">
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category "><a
                            href="https://www.ravsin.com.au/gaming/email-codes/" class="nuhover dropdown-toggle">Email
                            Codes</a>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="https://www.ravsin.com.au/gaming/ps3/" class="nuhover dropdown-toggle">PS3</a>
                        <ul class="sub-category">
                            <li class=""><a href="https://www.ravsin.com.au/gaming/ps3/accessories/">Accessories</a>
                            </li>
                        </ul>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="https://www.ravsin.com.au/gaming/ps4/" class="nuhover dropdown-toggle">PS4</a>
                        <ul class="sub-category">
                            <li class=""><a href="https://www.ravsin.com.au/gaming/ps4/accessories/">Accessories</a>
                            </li>
                        </ul>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="https://www.ravsin.com.au/gaming/xbox-360/" class="nuhover dropdown-toggle">Xbox
                            360</a>
                        <ul class="sub-category">
                            <li class=""><a
                                    href="https://www.ravsin.com.au/gaming/xbox-360/accessories/">Accessories</a></li>
                            <li class=""><a href="https://www.ravsin.com.au/gaming/xbox-360/xbox-live/">XBOX LIVE</a>
                            </li>
                        </ul>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="https://www.ravsin.com.au/gaming/xbox-one/" class="nuhover dropdown-toggle">Xbox
                            One</a>
                        <ul class="sub-category">
                            <li class=""><a
                                    href="https://www.ravsin.com.au/gaming/xbox-one/accessories/">Accessories</a></li>
                            <li class=""><a href="https://www.ravsin.com.au/gaming/xbox-one/xbox-live/">XBOX LIVE</a>
                            </li>
                        </ul>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="https://www.ravsin.com.au/gaming/ps-vita/" class="nuhover dropdown-toggle">PS VITA</a>
                        <ul class="sub-category">
                            <li class=""><a href="https://www.ravsin.com.au/gaming-accessories/ps-vita/accessories/">Accessories</a>
                            </li>
                        </ul>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category back-menu"><a
                            href="https://www.ravsin.com.au/gaming-accessories/nintendo/"
                            class="nuhover dropdown-toggle">Nintendo</a>
                        <ul class="sub-category">
                            <li class=""><a href="https://www.ravsin.com.au/gaming-accessories/nintendo/accessories/">Accessories</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="dropdownhassub">
                <a href="https://www.ravsin.com.au/health-beauty/" class="dropdown-toggle">Health
                    & Beauty</a>
                <div class="after-div-lv2"></div>
                <ul class="dropdown-menu">
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category "><a
                            href="https://www.ravsin.com.au/health-beauty/pain-relief/" class="nuhover dropdown-toggle">Pain
                            Relief</a>
                    </li>
                    <li class="col-xs-12 col-sm-6 col-md-3 second-category "><a
                            href="https://www.ravsin.com.au/health-beauty/skin-care/" class="nuhover dropdown-toggle">Skin
                            Care</a>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="topmenucustom">
            <ul>
                <li>
                    <a href="#"><i class="fa fa-tag" aria-hidden="true"></i> Thương hiệu</a>
                </li>
                <li>
                    <a href="#"><i
                            class="fa fa-plane" aria-hidden="true"></i> Hàng mới</a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-star" aria-hidden="true"></i> Bán chạy</a>
                </li>
                <li class="hotdeals">
                    <a href="#"><i class="fa fa-fire"
                                   aria-hidden="true"></i>
                        Ưu đãi</a>
                </li>
            </ul>
        </div>
        <div class="browsinghistory mb">
            <div class="tooltip"><i class="fa fa-history" aria-hidden="true"></i> Browsing History
                <span class="tooltiptext">
<span nloader-content-id="19b7e42fccf26ee4cc5b06dbbeaf5925647bbf89568690141064a029c81b584d"
      nloader-content="WyVwcm9kdWN0X2hpc3RvcnkgbGltaXQ6J1tAY29uZmlnOnBoaXN0b3J5X2xpbWl0QF0nIHRlbXBsYXRlOicnJV1bJXBhcmFtIGhlYWRlciVdPGhyIC8-DQoJCQkJCQkJCQkJCQk8aDM-WW91ciBCcm93c2luZyBIaXN0b3J5PC9oMz4NCgkJCQkJCQkJCQkJCTxiciAvPg0KCQkJCQkJCQkJCQkJPGRpdiBjbGFzcz0icm93IGN1c3RvbXByb2R1Y3RzaW5mb290ZXIiPlslL3BhcmFtJV1bJXBhcmFtIGZvb3RlciVdPC9kaXY-WyUvcGFyYW0lXVslL3Byb2R1Y3RfaGlzdG9yeSVd"
      nloader-data="eyJwYWdlX2hlYWRlciI6bnVsbCwiY29tcGF0Y2F0X2Rlc2NyaXB0aW9uMiI6bnVsbCwiZGVzY3JpcHRpb24yIjpudWxsLCJjb250ZW50IjpudWxsLCJjYXRlZ29yeV9uYW1lIjpudWxsLCJjYXRlZ29yeV9yZWYiOm51bGwsImJsb2dfYXV0aG9yIjpudWxsLCJjb250ZW50X3Nob3J0X2Rlc2NyaXB0aW9uMSI6IiIsImNvbnRlbnRfbGFiZWwyIjoiIiwicGFnZV9zdWJoZWFkZXIiOm51bGwsImNvbXBhdGNhdF9uYW1lIjpudWxsLCJ1cmwiOiJodHRwczovL3d3dy5yYXZzaW4uY29tLmF1LyIsInRlbXBsYXRlc2VhcmNoIjoic2VhcmNoX3Jlc3VsdHMiLCJjb250ZW50X2xldmVsIjoiMSIsImNvbnRlbnRfbW9kdWxlIjoiIiwiY29udGVudF9mdWxscGF0aCI6IkhvbWUiLCJyZXZpZXdzIjoiMCIsIm5hbWUiOm51bGwsImRlc2NyaXB0aW9uIjpudWxsLCJ0ZW1wbGF0ZWJvZHkiOiJob21lIiwicGFyZW50X2NvbnRlbnRfaWQiOiIwIiwiY29udGVudF9zaG9ydF9kZXNjcmlwdGlvbjMiOiIiLCJibG9nX3N1YmplY3QiOm51bGwsImNvbnRlbnRfdHlwZV9jb2RlIjoicGFnZSIsInRpdGxlIjpudWxsLCJjb250ZW50X3d1Zm9vX2Zvcm0iOiIiLCJhcnRpY2xlX2lkIjpudWxsLCJjb250ZW50X2lkIjoiMzkiLCJjb250ZW50X2xhYmVsMSI6IiIsImNvbnRlbnRfbGFiZWwzIjoiIiwiY2F0ZWdvcnlfaWQiOm51bGwsInRlbXBsYXRlZm9vdCI6IiIsInRpbWVzdGFtcCI6IjIwMTgtMDctMjQgMTA6MTg6MzMiLCJjb250ZW50X2V4dGVybmFsX3JlZjMiOiIiLCJjY2F0X2lkIjpudWxsLCJjb250ZW50X2Rlc2NyaXB0aW9uMSI6IjxwPiZuYnNwOzwvcD5cclxuXHJcbjxociAvPlxyXG48aDEgc3R5bGU9XCJmb250LXNpemU6MjhweDtcIj5EZWxpdmVyaW5nIHRoZSBCZXN0IGluIFRlY2gsIEdhbWluZyBhbmQgQXVkaW8gYW5kIFZpZGVvPC9oMT5cclxuXHJcbjxwPkl04oCZcyBuZXZlciBiZWVuIGVhc2llciB0byBnZXQgdGhlIHByb2R1Y3RzIHlvdSBuZWVkIGF0IGEgcHJpY2UgeW914oCZbGwgbG92ZS4gUmF2c2luIGlzIGFuIEF1c3RyYWxpYW4tb3duZWQgcmV0YWlsZXIgaGVscGluZyBwZW9wbGUgYWNyb3NzIHRoZSBjb3VudHJ5IGFjY2VzcyB0aGUgbGF0ZXN0IGFuZCBncmVhdGVzdCBpbiB0ZWNobm9sb2d5IGFuZCBlbGVjdHJvbmljcy4gV2UgdW5kZXJzdGFuZCB0aGF0IHdoZW4gaXTigJlzIHRpbWUgdG8gdXBncmFkZSwgeW91IHdhbnQgdGhlIGZhc3Rlc3QsIHRoZSBiaWdnZXN0IGFuZCB0aGUgYmVzdCBhdCB0aGUgbW9zdCBjb21wZXRpdGl2ZSBwcmljZXMuIEdldCBhIGdyZWF0IGRlYWwgd2l0aG91dCBjb21wcm9taXNpbmcgb24gdGhlIHF1YWxpdHkgb2YgeW91ciBuZXcgdG95IGJ5IHNob3BwaW5nIG91ciByYW5nZS48L3A-XHJcblxyXG48cD5BIHRydWx5IG5hdGlvbndpZGUgY291bnRyeSwgd2UgbWFrZSBpdCBzaW1wbGUgZm9yIGV2ZXJ5IEF1c3RyYWxpYW4gdG8gZ2V0IHRoZSBwcm9kdWN0cyB0aGV5IGxvdmUgc29vbmVyLCBub3QgbGF0ZXIuIFdpdGggYSByYW5nZSBvZiBwb3N0YWdlIG9wdGlvbnMgYW5kIGRpc3BhdGNoaW5nIG9uIGF2ZXJhZ2UgaW4gdW5kZXIgMjQgaG91cnMsIHlvdeKAmWxsIGJlIGFibGUgdG8gZW5qb3kgeW91ciBuZXcgcHJvZHVjdHMgaW4gZGF5cywgbm90IHdlZWtzLiBEaXNjb3ZlciB3aGF0IHNvIG1hbnkgcGVvcGxlIGFscmVhZHkga25vdyBhbmQgb3JkZXIgZnJvbSBSYXZzaW4gdG9kYXkhPC9wPlxyXG5cclxuPGgyIHN0eWxlPVwiZm9udC1zaXplOjI0cHg7XCI-RXZlcnl0aGluZyB5b3UgbmVlZCBmb3IgYSBzbWFydGVyLCBtb3JlIGNvbWZvcnRhYmxlIGxpZmU8L2gyPlxyXG5cclxuPHA-T3VyIGRpdmVyc2UgcHJvZHVjdCBvZmZlcmluZyBoYXMgc29tZXRoaW5nIGZvciBldmVyeW9uZS4gV2hldGhlciB5b3XigJlyZSBhIGJ1c2luZXNzLW93bmVyIGxvb2tpbmcgZm9yIGFuIGFmZm9yZGFibGUgc291cmNlIG9mIHByaW50ZXIgY2FydHJpZGdlcyBvciBhIGdhbWVyIHdobyB3YW50cyB0byBlbmpveSB0aGUgbGF0ZXN0IGFuZCBncmVhdGVzdCBYYm94IGFuZCBQbGF5c3RhdGlvbiB0aXRsZXMsIHdl4oCZdmUgZ290IHNvbWV0aGluZyBmb3IgeW91LjwvcD5cclxuXHJcbjxwPk91ciByYW5nZSBvZiBtb2JpbGUgcGhvbmUgYWNjZXNzb3JpZXMgaW5jbHVkaW5nIDxhIGhyZWY9XCJodHRwczovL3d3dy5yYXZzaW4uY29tLmF1L2VsZWN0cm9uaWNzL21vYmlsZS1waG9uZXMvY2FzZXMtY292ZXJzL1wiPmNhc2VzPC9hPiBhbmQgPGEgaHJlZj1cImh0dHBzOi8vd3d3LnJhdnNpbi5jb20uYXUvZWxlY3Ryb25pY3MvbW9iaWxlLXBob25lcy9zY3JlZW4tcHJvdGVjdG9ycy9cIj5zY3JlZW4gcHJvdGVjdG9yczwvYT4gaGVscHMgeW91IGtlZXAgeW91ciBtb3N0IHByZWNpb3VzIGRldmljZSBzYWZlIGFuZCBzZWN1cmUuIExvb2tpbmcgdG8gY2FycnkgbW9yZSBzb25ncywgbW9yZSBnYW1lcywgbW9yZSBtb3ZpZXMsIG1vcmUgcGhvdG9zPyBPdXIgaGlnaC1kZW5zaXR5IHN0b3JhZ2Ugc29sdXRpb25zIGluY2x1ZGluZyBzb2xpZCBzdGF0ZSBkcml2ZXMgYW5kIDxhIGhyZWY9XCJodHRwczovL3d3dy5yYXZzaW4uY29tLmF1L21lbW9yeS1zdG9yYWdlL21lbW9yeS1jYXJkcy9cIj5tZW1vcnkgY2FyZHM8L2E-IGFyZSB0aGUgcGVyZmVjdCBjaG9pY2UsIG9mZmVyaW5nIGh1bmRyZWRzIG9mIGdpZ2FieXRlcyBvZiBzcGFjZSBpbiBleHRyZW1lbHkgcG9ydGFibGUgZm9ybWF0cy48L3A-XHJcblxyXG48cD5Gb3IgdGhvc2UgbG9va2luZyB0byB1cGdyYWRlLCB0YWtlIGEgbG9vayZuYnNwOyBhdCBvdXIgcmFuZ2Ugb2Ygc21hcnQgd2F0Y2hlcywgdGFibGV0cywgTVAzIHBsYXllcnMgYW5kIGRpZ2l0YWwgY2FtZXJhcywgYWxsb3dpbmcgeW91IHRvIGdldCB0aGUgc3RhdGUtb2YtdGhlLWFydCBhdCBhbiBleHRyZW1lbHkgYWZmb3JkYWJsZSBwcmljZS4gV2hldGhlciB5b3XigJlyZSBhIGJ1ZGRpbmcgcGhvdG9ncmFwaGVyLCBhbiBleHRyZW1lIHNwb3J0cyBqdW5raWUgb3IganVzdCBzb21lb25lIHdobyBuZXZlciB3YW50cyB0byBtaXNzIGEgbm90aWZpY2F0aW9uIGFnYWluLCBicm93c2Ugb3VyIHJhbmdlIGFuZCBmaW5kIHNvbWV0aGluZyBmb3IgeW91LjwvcD5cclxuXHJcbjxoMz5UaGUgcmlnaHQgY2hvaWNlIGZvciB5b3U8L2gzPlxyXG5cclxuPHA-UGxhY2UgeW91ciBvcmRlciB0b2RheSBhbmQgZmluZCBvdXQgd2h5IHNvIG1hbnkgcGVvcGxlIGFjcm9zcyB0aGUgY291bnRyeSBjaG9vc2UgUmF2c2luIGZvciB0aGVpciB0ZWNoIG5lZWRzLiBBbnkgcXVlc3Rpb25zPyBHZXQgaW4gdG91Y2ggdmlhIDxhIGhyZWY9XCJodHRwczovL3d3dy5yYXZzaW4uY29tLmF1L2NvbnRhY3QtdXNcIj5vdXIgZW5xdWlyeSBmb3JtPC9hPiBhbmQgb3VyIGZyaWVuZGx5IHN0YWZmIHdpbGwgZ2l2ZSB5b3UgdGhlIGFuc3dlcnMgeW91IG5lZWQgYXMgc29vbiBhcyBwb3NzaWJsZS4mbmJzcDs8L3A-XHJcblxyXG48cD48c2NyaXB0IHR5cGU9XCJhcHBsaWNhdGlvbi9sZCtqc29uXCI-XHJcbntcclxuICBcIkBjb250ZXh0XCI6IFwiaHR0cDovL3NjaGVtYS5vcmdcIixcclxuICBcIkB0eXBlXCI6IFwiV2ViU2l0ZVwiLFxyXG4gIFwidXJsXCI6IFwiaHR0cHM6Ly93d3cucmF2c2luLmNvbS9cIixcclxuICBcInBvdGVudGlhbEFjdGlvblwiOiB7XHJcbiAgICBcIkB0eXBlXCI6IFwiU2VhcmNoQWN0aW9uXCIsXHJcbiAgICBcInRhcmdldFwiOiBcImh0dHBzOi8vd3d3LnJhdnNpbi5jb20vP3JmPWt3Jmt3PVwiLFxyXG4gICAgXCJxdWVyeS1pbnB1dFwiOiBcInJlcXVpcmVkIG5hbWU9c2VhcmNoX3Rlcm1fc3RyaW5nXCJcclxuICB9XHJcbn1cclxuPC9zY3JpcHQ-PC9wPlxyXG4iLCJwYWdlX2lkIjpudWxsLCJkYXRlX3VwZGF0ZWQiOm51bGwsInRlbXBsYXRlaGVhZCI6IiIsImNvbnRlbnRfc2hvcnRfZGVzY3JpcHRpb24yIjoiIiwiZGF0ZV9wb3N0ZWQiOiIwMDAwLTAwLTAwIDAwOjAwOjAwIiwiY29udGVudF9leHRlcm5hbF9yZWYxIjoiIiwiY29tcGF0X2xpc3RfaWQiOm51bGwsImNvbnRlbnRfbmFtZSI6IkhvbWUiLCJjb250ZW50X2V4dGVybmFsX3JlZjIiOiIwIiwicGFnZV9lZGl0b3IiOm51bGwsImJsb2dfc3ViaGVhZGVyIjpudWxsLCJjb250ZW50X3R5cGVfbmFtZSI6IkluZm9ybWF0aW9uIFBhZ2UiLCJ0aHVtYiI6bnVsbCwiY29tcGF0Y2F0X3JlZiI6bnVsbCwiY29udGVudF9hdXRob3IiOiIiLCJwYXJlbnRfaWQiOmZhbHNlLCJwYWdlX2NvbnRlbnQiOm51bGwsImNvbnRlbnRfYWxsb3dfcmV2aWV3cyI6Im4iLCJjb250ZW50X3JlZiI6ImhvbWUiLCJibG9nX2lkIjpudWxsLCJpZCI6bnVsbCwiY29udGVudF9leHRlcm5hbF9zcmMiOiJIb21lIFBhZ2UgKHY0LjUpIiwidGh1bWJfYWx0MSI6bnVsbCwiY2F0ZWdvcnlfc3ViaGVhZGVyIjpudWxsLCJibG9nX2NvbnRlbnQiOm51bGwsInNob3J0X2Rlc2NyaXB0aW9uIjpudWxsLCJhY2Nlc3NfY29udHJvbCI6IiIsImNvbnRlbnRfY29tcGF0aWJpbGl0eV9jb2RlIjoibiIsImNvbnRlbnRfdHlwZV9pZCI6IjIiLCJjb250ZW50X2Rlc2NyaXB0aW9uMiI6IiIsImNvbXBhdGNhdF9kZXNjcmlwdGlvbiI6bnVsbCwicGFnZV9pbmRleCI6bnVsbCwicmF0aW5nIjoiMCIsImNvbnRlbnRfZGVzY3JpcHRpb24zIjoiIiwidGh1bWJfYWx0IjpudWxsLCJjb21wYXRfaWQiOm51bGwsImdwX3Jlc3RyaWN0aW9uIjoiMCIsInBhcmVudF9jY2F0X2lkIjpmYWxzZSwiY29tcGF0Y2F0X2Z1bGxuYW1lIjpudWxsLCJleHRlcm5hbF9yZWYiOm51bGwsInNvcnRvcmRlciI6IjAiLCJzdWJ0aXRsZSI6bnVsbCwidGh1bWJfY29udGVudF90eXBlX2lkIjoiMCJ9"></span>
</span>
            </div>
        </div>
        <a href="https://www.ravsin.com.au/contact-us" title="Contact" class="contact-menu-mobile">Contact Us</a>
    </div>
    <div class="wrapper-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4 wrapper-logo">
                    <div class="icon-menu-mobile icon-click" data-click="#menu-mobile"></div>
                    <a href="#" title="S-Shoe">
                        <img class="logo" src="/images/logo.png" alt="S-Shoe"/>
                    </a>
                </div>
                <div class="after-search"></div>
                <div class="col-sm-4 customsearchbar">
                    <form name="productsearch" method="get" action="/" class="navbar-form">
                        <input type="hidden" name="rf" value="kw"/>
                        <div class="input-group">
                            <input class="form-control ajax_search" placeholder="Tìm kiếm ..." value=""
                                   id="name_search" autocomplete="off" name="kw" type="text"/>
                            <span class="input-group-btn">
                                <button type="submit" value="Search" class="btn btn-default"><i
                                        class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
                <a href="#" title="user" class="user-mobile hidden-md hidden-sm hidden-lg"><i
                        class="fa fa-user"></i></a>
                <div class="col-xs-12 col-sm-2 col-sm-2-custom">
                    <div class="contact-cart-container">
                        <a href="#" title="Contact us" class="top-contact">Liên hệ</a>
                        <ul class="nav navbar-nav navbar-right customcart">
                            <li class="navbar-right floatleft">
                                <div id="header-cart" class="btn-group">
                                    <a href="#"
                                       class="btn btn-default" id="cartcontentsheader">
                                        <i class="fa fa-shopping-cart"></i><span class="visible-lg visible-inline-lg"> Giỏ hàng -</span>
                                        <span rel="a2c_item_count">(0)</span>
                                        <span rel="a2c_sub_total" class="cartsub hidden-xs">00đ</span>
                                    </a>
                                    <a href="#" class="btn btn-default dropdown-toggle hidden-xs hidden-md hidden-sm"
                                       data-toggle="dropdown"><span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li class="box" id="neto-dropdown">
                                            <div class="body padding" id="cartcontents"></div>
                                            <div class="footer"></div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper-navigation hidden-md hidden-xs hidden-sm">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="navbar navbar-default menu-desktop">
                        <h3 class="title-menu-cat">Thể loại</h3>

                        @if (Route::getCurrentRoute()->uri() == '/')
                            <div class="navbar-collapse collapse navbar-responsive-collapse active">
                        @else
                             <div class="navbar-collapse collapse navbar-responsive-collapse ">
                        @endif
                            <ul class="nav navbar-nav">
                                <?php
                                foreach ($categories as $category) { ?>
                                    <input type="hidden" value=""/>
                                    <li class="dropdown dropdown-hover">
                                        <a href="#"
                                           class="dropdown-toggle">{{$category->name}}</a>
                                        <ul class="dropdown-menu">
                                            <?php foreach ($categoryDetails as $categoryDetail){
                                                if($category->category_id == $categoryDetail->parent_id) { ?>
                                                    <li class="dropdown dropdown-hovercol-xs-12 col-sm-6 col-md-3 second-category">
                                                        <a href="#" class="nuhover dropdown-toggle">
                                                            {{$categoryDetail->name}}
                                                        </a>
                                                        <ul class="sub-category">
                                                            <li class=""><a
                                                                    href="https://www.ravsin.com.au/memory-storage/memory-cards/cf-cards/">CF
                                                                    Cards</a></li>
                                                            <li class=""><a href="https://www.ravsin.com.au/microsd-cards/">microSD
                                                                    Cards</a></li>
                                                            <li class=""><a href="https://www.ravsin.com.au/sd-cards/">SD Cards</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                <?php } } ?>
                                        </ul>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="topmenucustom">
                        <li>
                            <a href="#"><i class="fa fa-tag" aria-hidden="true"></i> Thương hiệu</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-plane" aria-hidden="true"></i> Hàng mới</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-star" aria-hidden="true"></i> Bán chạy</a>
                        </li>
                        <li class="hotdeals">
                            <a href="#"><i class="fa fa-fire" aria-hidden="true"></i> Ưu đãi</a>
                        </li>
                    </div>
                    <div class="browsinghistory">
                        <div class="tooltip">Browsing History <i class="fa fa-history" aria-hidden="true"></i>
                            <span class="tooltiptext"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-12 customsearchbar visible-xs">
    <form name="productsearch" method="get" action="/" class="navbar-form">
        <input type="hidden" name="rf" value="kw"/>
        <div class="input-group">
            <input class="form-control ajax_search" placeholder="Tìm kiếm ..." value="" id="name_search" autocomplete="off" name="kw" type="text"/>
            <span class="input-group-btn">
                <button type="submit" value="Search" class="btn btn-default"><i class="fa fa-search"></i></button>
            </span>
        </div>
    </form>
</div>
