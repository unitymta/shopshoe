@extends('layouts.master')

@section('content')

    @if ( Session::has('success') )
        <div class="alert alert-success alert-dismissible" role="alert">
            <strong>{{ Session::get('success') }}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
        </div>
    @endif

    @if ( Session::has('error') )
        <div class="alert alert-danger alert-dismissible" role="alert">
            <strong>{{ Session::get('error') }}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
        </div>
    @endif

    <div class="row">
        <div class="col-xs-12">
            <div class="page-header">
                <h1>Tài khoản</h1>
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="well">
                        <form id="login" method="post" action="{{ url('/login') }}">
                            @csrf
                            <input type="hidden" name="csrf_token" value="">
                            <input type="hidden" name="redirect" value="">
                            <h3>Đăng Nhập</h3>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group ">
                                        <label>Email</label>
                                        <input type="text" name="email" class="form-control" value="" size="25" placeholder="sangtx@example.com">
                                        <p class="help-block hidden"><i class="fa fa-question-circle"></i> Quên tên đăng nhập <a href="/forgotusr">Bấm vào đây</a></p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label>Mật Khẩu</label>
                                        <input type="password" name="password" class="form-control" value="" size="25">
                                        <p class="help-block"><i class="fa fa-question-circle"></i> Quên mật khẩu <a href="/forgotpwd">Bấm vào đây</a></p>
                                    </div>
                                </div>
                            </div>
                            <button name="submit" type="submit" class="btn btn-lg btn-success btn-block">
                                <i class="fa fa-user"></i> Đăng Nhập
                            </button>
                        </form>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="well">
                        <form id="register" method="post" action="{{ url('/register') }}">
                            @csrf
                            <input type="hidden" name="csrf_token" value="">
                            <input type="hidden" name="redirect" value="">
                            <h3>Đăng Ký</h3>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <span class="text-danger small">Yêu cầu</span>
                                        <input class="form-control" type="email" name="reg_email" value="" size="25">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label>Mật khẩu</label>
                                        <span class="text-danger small">Yêu cầu</span>
                                        <input class="form-control" type="password" name="password" value="" size="25">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label>Nhập lại mật khẩu</label>
                                        <span class="text-danger small">Yêu cầu</span>
                                        <input class="form-control" type="password" name="password_confirmation" value="" size="25">
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input name="regoptin" type="checkbox" value="Y" checked="checked" data-message="Bạn có thể hủy theo dõi bất cứ lúc nào.">
                                                Đăng ký để nhận tin mới nhất.
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                        <input name="fn" type="hidden" value="proc">
                                        <button name="submit" type="submit" class="btn btn-lg btn-block btn-success"><i
                                                class="fa fa-plus"></i> Đăng Ký
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End #main-content + .row -->
@endsection
