<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{$pageTitle}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('bower_components/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('bower_components/Ionicons/css/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('bower_components/admin-lte/dist/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('bower_components/admin-lte/dist/css/skins/_all-skins.min.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
        .profile-user-img {
            height: 100px;
        }
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

@include('admin.block.header')

<!-- Left side column. contains the logo and sidebar -->
@include('admin.block.sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Thông tin người dùng
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{url('admin/user/list')}}">Danh sách</a></li>
                <li class="active">Người dùng</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-3">

                    <!-- Profile Image -->
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <img class="profile-user-img img-responsive img-circle"
                                 src="/images/avatar/{{$getUserProfile->image}}" alt="{{$getUserProfile->given_name}}">

                            <h3 class="profile-username text-center">{{$getUserProfile->given_name}}</h3>

                            <p class="text-muted text-center"><i
                                    class="fa fa-paper-plane"></i> {{$getUserProfile->email}} &nbsp;&nbsp;&nbsp; <i
                                    class="fa fa-phone-square"></i> {{$getUserProfile->mobile_phone}}</p>

                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>Số bình luận</b> <a class="pull-right">1,322</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Số đơn đặt hàng</b> <a class="pull-right">11</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Tổng số tiền đã chi</b> <a class="pull-right">10.000.000 VND</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Lần đăng nhập gần nhất</b> <a class="pull-right">2019-09-01</a>
                                </li>
                            </ul>

                            <a href="#" class="btn btn-primary btn-block"><b>Thống kê chi tiết</b></a>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                    <!-- About Me Box -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thông tin</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <strong><i class="fa fa-book margin-r-5"></i> Giới thiệu</strong>
                            <p class="text-muted">{{$getUserProfile->description}}</p>
                            <hr>
                            <strong><i class="fa fa-map-marker margin-r-5"></i> Địa chỉ</strong>
                            <p class="text-muted">{{$getUserProfile->address}}</p>
                            <hr>
                            <strong><i class="fa fa-birthday-cake"></i> Sinh nhật</strong>
                            <p class="text-muted">{{$getUserProfile->birthday}}</p>
                            <hr>
                            <strong><i class="fa fa-calendar-times-o"></i> Ngày tham gia</strong>
                            <p class="text-muted">{{$getUserProfile->created_at}}</p>
                            <hr>
                            <strong><i class="fa fa-star margin-r-5"></i> Hạng thành viên</strong>
                            <p>
                                <span class="label label-success">Kim cương</span>
                                <span class="label label-info">Bạch kim</span>
                                <span class="label label-warning">Vàng</span>
                                <span class="label label-primary">Bạc</span>
                            </p>
                            <hr>

                            <strong><i class="fa fa-file-text-o margin-r-5"></i> Ghi chú</strong>
                            <p>Khách hàng cực kỳ vui tính, thân thiện, dễ gần, cởi mở, thông minh, hài hước : ))</p>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#activity" data-toggle="tab">Hoạt động</a></li>
                            <li><a href="#settings" data-toggle="tab">Cập nhật thông tin</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="active tab-pane" id="activity">
                                <!-- Post -->
                                <div class="post box box-widget">
                                    <div class="box-header with-border">
                                        <div class="user-block">
                                            <img class="img-circle img-bordered-sm" src="/images/combo-1.jpg" alt="">
                                            <span class="username">
                                            <a href="#">Sang 1</a>
                                        </span>
                                            <span class="description">7:30 hôm nay</span>
                                        </div>
                                        <!-- /.user-block -->
                                        <div class="box-tools">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <p>
                                        Hàng đẹp, giá tốt. CÒn ủng hộ dài dài
                                    </p>
                                    <ul class="list-inline">
                                        <li>
                                            <a href="#" class="link-black text-sm"><i
                                                    class="fa fa-thumbs-o-up margin-r-5"></i> Thích</a>
                                        </li>
                                        <li class="pull-right text-muted">
                                            45 thích - 2 bình luận
                                        </li>
                                    </ul>
                                    <div class="box-footer box-comments" style="margin-top: 10px; margin-bottom: 10px;">
                                        <div class="box-comment">
                                            <!-- User image -->
                                            <img class="img-circle img-sm" src="/images/banner.jpg" alt="User Image">
                                            <div class="comment-text">
                                                <span class="username">ssang 2
                                                    <span class="text-muted pull-right">8:03 Hôm nay</span>
                                                </span><!-- /.username -->
                                                Đúng
                                            </div>
                                            <!-- /.comment-text -->
                                        </div>
                                        <!-- /.box-comment -->
                                        <div class="box-comment">
                                            <!-- User image -->
                                            <img class="img-circle img-sm" src="/images/banner2.jpg" alt="User Image">
                                            <div class="comment-text">
                                                <span class="username">unity
                                                    <span class="text-muted pull-right">8:03 2019-09-09</span>
                                                </span><!-- /.username -->
                                                hahahaha                                           </div>
                                            <!-- /.comment-text -->
                                        </div>
                                        <!-- /.box-comment -->
                                    </div>
                                    <form class="form-horizontal">
                                        <div class="form-group margin-bottom-none">
                                            <div class="col-sm-9">
                                                <input style="margin-bottom: 10px" class="form-control input-sm"
                                                       placeholder="Nhập bình luận">
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <button style="margin-bottom: 10px" type="submit"
                                                                class="btn btn-primary pull-right btn-block btn-sm">Gửi
                                                        </button>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <button style="margin-bottom: 10px" type="submit"
                                                                class="btn btn-danger pull-right btn-block btn-sm">Xóa
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.post -->
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="settings">
                                <form class="form-horizontal" name="updateProfile"
                                      action="{{url('admin/user/profile')}}/{{$getUserProfile->id}}" method="post"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="name" class="col-sm-2 control-label">Tên</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="name" id="name"
                                                   placeholder="{{$getUserProfile->given_name}}"
                                                   value="{{$getUserProfile->given_name}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="col-sm-2 control-label">Email</label>

                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" name="email" id="email"
                                                   placeholder="{{$getUserProfile->email}}"
                                                   value="{{$getUserProfile->email}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="address" class="col-sm-2 control-label">Địa chỉ</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="address" id="address"
                                                   placeholder="{{$getUserProfile->address}}"
                                                   value="{{$getUserProfile->address}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone" class="col-sm-2 control-label">Số điện thoại</label>

                                        <div class="col-sm-10">
                                            <input type="number" class="form-control" name="phone" id="phone"
                                                   placeholder="{{$getUserProfile->mobile_phone}}"
                                                   value="{{$getUserProfile->mobile_phone}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="description" class="col-sm-2 control-label">Giới thiệu</label>

                                        <div class="col-sm-10">
                                            <textarea class="form-control" name="description" id="description"
                                                      placeholder="{{$getUserProfile->description}}"
                                                      value="{{$getUserProfile->description}}">{{$getUserProfile->description}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="birthday" class="col-sm-2 control-label">Ngày sinh</label>

                                        <div class="col-sm-10">
                                            <input type="date" class="form-control" name="birthday" id="birthday"
                                                   value="{{$getUserProfile->birthday}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="avatar" class="col-sm-2 control-label">Ảnh đại diện</label>

                                        <div class="col-sm-10">
                                            <input type="file" id="image" name="image" accept="image/png, image/jpeg">
                                            <img id="previewImage" src="#" alt=""
                                                 style="    width: 300px;margin: 10px;display: block;"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-primary">Cập nhật</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@include('admin.block.footer')

<!-- Control Sidebar -->
@include('admin.block.option')
<!-- /.control-sidebar -->

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('bower_components/admin-lte/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('bower_components/admin-lte/dist/js/demo.js')}}"></script>

<script>
    $(document).ready(function () {
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#previewImage').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#image").change(function () {
            readURL(this);
        });
    });
</script>
</body>
</html>
