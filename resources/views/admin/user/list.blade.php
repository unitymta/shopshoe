<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{$pageTitle}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('bower_components/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('bower_components/Ionicons/css/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('bower_components/admin-lte/dist/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('bower_components/admin-lte/dist/css/skins/_all-skins.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('bower_components/admin-lte/plugins/iCheck/flat/blue.css')}}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
        .pagination {
            margin: 0;
            float: right;
        }

        .mailbox-controls {
            padding: 10px 5px;
        }

        .modal-open .modal {
            padding: 0;
        }

        .modal-dialog {
            position: absolute;
            left: 50%;
            top: 50%;
            width: 1280px !important;
            margin: 50px auto;
        }

        .modal.in .modal-dialog {
            transform: translate(-50%, -50%);
        }

        table tr td input {
            border: 0;
        }

        .table-striped > tbody > tr:nth-of-type(even) {
            background-color: #f9f9f9;
        }

        @media (max-width: 1279px) {
            .modal-dialog {
                width: 750px !important;
                margin: 50px auto;
            }
        }

        @media (max-width: 767px) {
            .modal-dialog {
                width: 750px !important;
                margin: 50px auto;
            }
        }

        @media (max-width: 767px) {
            .modal-dialog {
                width: 350px !important;
            }
        }
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

@include('admin.block.header')

<!-- Left side column. contains the logo and sidebar -->
@include('admin.block.sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Danh sách người sử dụng</h1>
            <ol class="breadcrumb">
                <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Danh sách</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-3">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Nhóm người dùng</h3>
                            <div class="box-tools">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body no-padding">
                            <ul class="nav nav-pills nav-stacked">
                                <li class="{{request()->get('type') == '' ? 'active' : ''}}">
                                    <a href="{{url('/admin/user/list')}}"><i class="fa fa-group"></i> Tất cả<span
                                            class="label label-primary pull-right">{{$count[0]->countAll}}</span></a>
                                </li>
                                <li class="{{request()->get('type') == 'admin' ? 'active' : ''}}">
                                    <a href="{{url('/admin/user/list?type=admin')}}"><i class="fa fa-user-secret"></i>
                                        Quản trị<span
                                            class="label label-primary pull-right">{{$count[0]->countAdmin}}</span></a>
                                </li>
                                <li class="{{request()->get('type') == 'user' ? 'active' : ''}}">
                                    <a href="{{url('/admin/user/list?type=user')}}"><i class="fa fa-user"></i> Người
                                        dùng<span
                                            class="label label-primary pull-right">{{$count[0]->countUser}}</span></a>
                                </li>
                                <li class="{{request()->get('type') == 'block' ? 'active' : ''}}">
                                    <a href="{{url('/admin/user/list?type=block')}}"><i class="fa fa-user-times"></i>
                                        Danh sách chặn<span
                                            class="label label-primary pull-right">{{$count[0]->countBlock}}</span></a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                @switch(request()->get('type'))
                                    @case('')
                                    Tất cả người sử dụng
                                    @break
                                    @case('admin')
                                    Quản trị viên
                                    @break
                                    @case('user')
                                    Người dùng
                                    @break
                                    @case('block')
                                    Danh sách bị chặn
                                    @break
                                @endswitch
                            </h3>
                            <div class="box-tools pull-right">
                                <div class="has-feedback">
                                    <input type="text" class="form-control input-sm form-controller" placeholder="Tìm kiếm" id="search" name="search">
                                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                </div>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <div class="mailbox-controls">
                                <!-- Check all button -->
                                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i
                                        class="fa fa-square-o"></i>
                                </button>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm delete"><i
                                            class="fa fa-trash-o"></i>
                                    </button>
                                    <button type="button" class="btn btn-default btn-sm edit" data-toggle="modal" data-target="#modal-edit">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                    <button type="button" class="btn btn-default btn-sm create" data-toggle="modal" data-target="#modal-create">
                                        <i class="fa  fa-plus"></i>
                                    </button>
                                </div>
                                <!-- /.btn-group -->
                                <button type="button" class="btn btn-default btn-sm refresh"><i
                                        class="fa fa-refresh"></i>
                                </button>
                                <div class="pull-right">
                                    {{$listAdmin->links()}}
                                </div>
                                <!-- /.pull-right -->
                            </div>
                            <div class="table-responsive mailbox-messages" id="result">
                                <table class="table table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Tên</th>
                                        <th>Địa chỉ</th>
                                        <th>Số điện thoại</th>
                                        <th>Email</th>
                                        <th>Ngày sinh</th>
                                        <th>Ngày tham gia</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($listAdmin as $key => $value)
                                        <tr>
                                            <td><input type="checkbox" id="userId{{$value->id}}" value="{{$value->id}}">
                                            </td>
                                            <td class="mailbox-name"><a
                                                    href="{{url('/admin/user/profile/'.$value->id)}}">{{$value->given_name}}</a>
                                            </td>
                                            <td class="mailbox-address">{{$value->address}}</td>
                                            <td class="mailbox-phone">{{$value->mobile_phone}}</td>
                                            <td class="mailbox-email">{{$value->email}}</td>
                                            <td class="mailbox-birthday">{{$value->birthday}}</td>
                                            <td class="mailbox-date-join">{{$value->created_at}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <!-- /.table -->
                            </div>
                            <!-- /.mail-box-messages -->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer no-padding">
                            <div class="mailbox-controls">
                                <!-- Check all button -->
                                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i
                                        class="fa fa-square-o"></i>
                                </button>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm delete"><i
                                            class="fa fa-trash-o"></i>
                                    </button>
                                    <button type="button" class="btn btn-default btn-sm edit" data-toggle="modal" data-target="#modal-edit">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                    <button type="button" class="btn btn-default btn-sm create" data-toggle="modal" data-target="#modal-create">
                                        <i class="fa  fa-plus"></i>
                                    </button>
                                </div>
                                <!-- /.btn-group -->
                                <button type="button" class="btn btn-default btn-sm refresh"><i
                                        class="fa fa-refresh"></i>
                                </button>
                                <div class="pull-right">
                                    {{$listAdmin->links()}}
                                </div>
                                <!-- /.pull-right -->
                            </div>
                        </div>
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal modal-info fade" id="modal-edit">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Chỉnh sửa người dùng</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive mailbox-messages">
                        <form action="">
                            <table class="table table-hover table-striped" style="color: #333;">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Tên</th>
                                    <th>Địa chỉ</th>
                                    <th>Số điện thoại</th>
                                    <th>Email</th>
                                    <th>Ngày sinh</th>
                                    <th>Ngày tham gia</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </form>
                        <!-- /.table -->
                    </div>
                    <!-- /.mail-box-messages -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Đóng</button>
                    <button type="button" class="btn btn-outline update">Lưu</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    @if ( Session::has('success') )
        <div class="alert alert-success alert-dismissible" role="alert">
            <strong>{{ Session::get('success') }}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
        </div>
    @endif

    @if ( Session::has('error') )
        <div class="alert alert-danger alert-dismissible" role="alert">
            <strong>{{ Session::get('error') }}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
        </div>
    @endif
    <div class="modal modal-info fade" id="modal-create">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Thêm mới người dùng</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive mailbox-messages">
                        <form action="">
                            <table class="table table-hover table-striped" style="color: #333;">
                                <thead>
                                <tr>
                                    <th>Tên</th>
                                    <th>Địa chỉ</th>
                                    <th>Số điện thoại</th>
                                    <th>Email</th>
                                    <th>Mật khẩu</th>
                                    <th>Ngày sinh</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="mailbox-name"><input name="name" type="text" value=""></td>
                                    <td class="mailbox-address"><input name="address" type="text" value=""></td>
                                    <td class="mailbox-phone"><input name="phone" type="text" value=""></td>
                                    <td class="mailbox-email"><input name="email" type="text" value=""></td>
                                    <td class="mailbox-email"><input name="password" type="text" value=""></td>
                                    <td class="mailbox-birthday"><input name="birthday" type="date" value=""></td>
                                </tr>
                                </tbody>
                            </table>
                        </form>
                        <!-- /.table -->
                    </div>
                    <!-- /.mail-box-messages -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Đóng</button>
                    <button type="button" class="btn btn-outline save">Lưu</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

@include('admin.block.footer')

<!-- Control Sidebar -->
@include('admin.block.option')
<!-- /.control-sidebar -->

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('bower_components/admin-lte/dist/js/adminlte.min.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('bower_components/admin-lte/plugins/iCheck/icheck.min.js')}}"></script>
<!-- Page Script -->
<script>
    $(function () {
        //Enable iCheck plugin for checkboxes
        //iCheck for checkbox and radio inputs
        $('.mailbox-messages input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue'
        });

        //Enable check and uncheck all functionality
        $(".checkbox-toggle").click(function () {
            var clicks = $(this).data('clicks');
            if (clicks) {
                //Uncheck all checkboxes
                $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
                $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
            } else {
                //Check all checkboxes
                $(".mailbox-messages input[type='checkbox']").iCheck("check");
                $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
            }
            $(this).data("clicks", !clicks);
        });

        //Handle starring for glyphicon and font awesome
        $(".mailbox-star").click(function (e) {
            e.preventDefault();
            //detect type
            var $this = $(this).find("a > i");
            var glyph = $this.hasClass("glyphicon");
            var fa = $this.hasClass("fa");

            //Switch states
            if (glyph) {
                $this.toggleClass("glyphicon-star");
                $this.toggleClass("glyphicon-star-empty");
            }

            if (fa) {
                $this.toggleClass("fa-star");
                $this.toggleClass("fa-star-o");
            }
        });

        // delete user
        $(".btn.delete").on("click", function () {
            var userChose = [];
            $.each($(".mailbox-messages input[type=\"checkbox\"]:checked"), function () {
                userChose.push($(this).val());
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                beforeSend: function (xhr, opts) {
                    if (userChose != '') {
                        if (confirm("Bạn chắc chắn muốn xóa người dùng này?")) {
                            $.each(userChose, function (index, value) {
                                $.ajax({
                                    type: "DELETE",
                                    url: value,
                                    success: function (data, textStatus) {
                                        location.reload();
                                    },
                                    error: function (xhr) {
                                        alert("Xảy ra lỗi. Vui lòng thử lại");
                                    }
                                });
                            });
                            return true;
                        } else {
                            xhr.abort();
                        }
                    } else {
                        alert("Xin vui lòng chọn người dùng muốn xóa.");
                    }
                }
            });
        });

        // edit user
        $(".btn.edit").on("click", function () {
            var userChose = [];
            $.each($(".mailbox-messages input[type=\"checkbox\"]:checked"), function () {
                userChose.push($(this).val());
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.each(userChose, function (index, value) {
                $.ajax({
                    type: "get",
                    url: value,
                    beforeSend: function () {
                        $(".modal-info table tbody").html("");
                    },
                    success: function (data, textStatus) {
                        $.each(data, function (index2, value2) {
                            $(".modal-info table tbody").append(`
                            <tr>
                                <td><input name="value-check${value2['id']}" type="hidden" id="${value2['id']}" value="${value2['id']}"></td>
                                <td class="mailbox-name"><input name="name${value2['id']}" type="text" value="${value2['given_name']}"></td>
                                <td class="mailbox-address"><input name="address${value2['id']}" type="text" value="${value2['address']}"></td>
                                <td class="mailbox-phone"><input name="phone${value2['id']}" type="text" value="${value2['mobile_phone']}"></td>
                                <td class="mailbox-email"><input name="email${value2['id']}" type="text" value="${value2['email']}"></td>
                                <td class="mailbox-birthday"><input name="birthday${value2['id']}" type="date" value="${value2['birthday']}"></td>
                                <td class="mailbox-date-join"><input name="date-join${value2['id']}" type="text" disabled value="${value2['created_at']}"></td>
                            </tr>
                        `);
                        });
                    },
                    error: function () {
                        alert("Xảy ra lỗi. Vui lòng thử lại");
                    }
                });
            });
        });

        // update user
        $(".btn.update").on("click", function () {
            var userChose = [];
            $.each($(".mailbox-messages input[type=\"hidden\"]"), function () {
                userChose.push($(this).val());
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.each(userChose, function (index, value) {
                $.ajax({
                    type: "put",
                    url: '{{URL::to("/admin/user")}}/' + value,
                    data: {
                        name: $(`input[name='name${value}']`).val(),
                        address: $(`input[name='address${value}']`).val(),
                        phone: $(`input[name='phone${value}']`).val(),
                        email: $(`input[name='email${value}']`).val(),
                        birthday: $(`input[name='birthday${value}']`).val()
                    },
                    success: function (data, textStatus) {
                        location.reload();
                    },
                    error: function () {
                        alert("Xảy ra lỗi. Vui lòng thử lại");
                    }
                });
            });
        });

        // create user
        $(".btn.save").on("click", function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "post",
                url: '{{URL::to("/admin/user/list")}}',
                data: {
                    name: $(`#modal-create input[name='name']`).val(),
                    address: $(`#modal-create input[name='address']`).val(),
                    phone: $(`#modal-create input[name='phone']`).val(),
                    email: $(`#modal-create input[name='email']`).val(),
                    password: $(`#modal-create input[name='password']`).val(),
                    birthday: $(`#modal-create input[name='birthday']`).val()
                },
                success: function (data, textStatus) {
                    location.reload();
                },
                error: function (jqXHR) {
                    alert("Xảy ra lỗi. Vui lòng thử lại");
                }
            });
        });

        $(".btn.refresh").on("click", function () {
            location.reload();
        });

        //search user
        $('#search').on('keyup', function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var query = $(this).val();
            var url = '{{URL::full()}}';

            if (url.split('?')[1] != null) {
                var pathUrl = url.split('?')[1].split('=');
                pathUrl.shift();
                pathUrl = pathUrl[0];
            } else {
                pathUrl = "";
            }

            if (query != '') {
                $.ajax({
                    url: '{{URL::to('admin/user/search')}}',
                    type: "POST",
                    data: {
                        query: query,
                        typeUser: pathUrl
                    },
                    success: function (data) {
                        $("#result table tbody").html('');
                        $.each(data, function (index, value) {
                            $("#result table tbody").append(`
                                        <tr>
                                            <td><input type="checkbox" id="userId${value['id']}" value="${value['id']}">
                                            </td>
                                            <td class="mailbox-name"><a
                                                    href="{{ url('/admin/user/profile') }}/${value['id']}">${value['given_name']}</a>
                                            </td>
                                            <td class="mailbox-address">${value['address']}</td>
                                            <td class="mailbox-phone">${value['mobile_phone']}</td>
                                            <td class="mailbox-email">${value['email']}</td>
                                            <td class="mailbox-birthday">${value['birthday']}</td>
                                            <td class="mailbox-date-join">${value['created_at']}</td>
                                        </tr>
                        `);
                        });
                    },
                    error: function () {
                        alert("Xảy ra lỗi. Vui lòng thử lại");
                    }
                });
            }
        });
    });
</script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('bower_components/admin-lte/dist/js/demo.js')}}"></script>
</body>
</html>
