<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{$pageTitle}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('bower_components/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('bower_components/Ionicons/css/ionicons.min.css')}}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('bower_components/admin-lte/dist/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('bower_components/admin-lte/dist/css/skins/_all-skins.min.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

@include('admin.block.header')

<!-- Left side column. contains the logo and sidebar -->
@include('admin.block.sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Danh sách sản phẩm
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Danh sách sản phẩm</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID Sản phẩm</th>
                                    <th>Mã sản phẩm</th>
                                    <th>Tên</th>
                                    <th>Tên đầy đủ</th>
                                    <th>Mô tả</th>
                                    <th>Hình ảnh</th>
                                    <th>Thể loại</th>
                                    <th>Cho phép sale</th>
                                    <th>Giảm giá</th>
                                    <th>Giá bán</th>
                                    <th>Giá nhập</th>
                                    <th>Đơn vị</th>
                                    <th>Hoạt động</th>
                                    <th>Chi nhánh</th>
                                    <th>Hàng tồn kho</th>
                                    <th>Số lượng</th>
                                    <th>Ngày tạo</th>
                                    <th>Ngày cập nhật</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($getProductList as $key=>$value)
                                    <tr>
                                        <td>{{$value->product_id}}</td>
                                        <td>{{$value->code}}</td>
                                        <td>{{$value->name}}</td>
                                        <td><a href="{{$value->id}}" title="{{$value->full_name}}">{{$value->full_name}}</a></td>
                                        <td>{{$value->description}}</td>
                                        <td>{{$value->images}}</td>
                                        <td>{{$value->category_name}}</td>
                                        <td>{{$value->allow_sale==1 ? "Có" : "Không"}}</td>
                                        <td>
                                            @if($value->discount_type==1)
                                                {{number_format($value->discount)}} %
                                            @elseif($value->discount_type==2)
                                                {{number_format($value->discount)}} VND
                                            @endif
                                        </td>
                                        <td>{{$value->price_base}}</td>
                                        <td>{{$value->price_cost}}</td>
                                        <td>{{$value->unit}}</td>
                                        <td><b>{{$value->is_active==1 ? "Có" : "Không"}}</b></td>
                                        <td>{{$value->branch_name}}</td>
                                        <td>{{$value->inventory}}</td>
                                        <td>{{$value->quantity}}</td>
                                        <td>{{$value->created_at}}</td>
                                        <td>{{$value->updated_at}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID Sản phẩm</th>
                                    <th>Mã sản phẩm</th>
                                    <th>Tên</th>
                                    <th>Tên đầy đủ</th>
                                    <th>Mô tả</th>
                                    <th>Hình ảnh</th>
                                    <th>Thể loại</th>
                                    <th>Cho phép sale</th>
                                    <th>Giảm giá</th>
                                    <th>Giá bán</th>
                                    <th>Giá nhập</th>
                                    <th>Đơn vị</th>
                                    <th>Hoạt động</th>
                                    <th>Chi nhánh</th>
                                    <th>Hàng tồn kho</th>
                                    <th>Số lượng</th>
                                    <th>Ngày tạo</th>
                                    <th>Ngày cập nhật</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@include('admin.block.footer')

<!-- Control Sidebar -->
@include('admin.block.option')
<!-- /.control-sidebar -->

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('bower_components/admin-lte/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('bower_components/admin-lte/dist/js/demo.js')}}"></script>
<!-- page script -->

<script>
    $(function () {
        $('#example1').DataTable();
        $('#example1_wrapper').css('overflow', 'scroll');

        //search product
        $('input[type="search"].form-control.input-sm').on('keyup', function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var query = $(this).val();

            if (query != '') {
                $.ajax({
                    url: '{{URL::to('admin/product/search')}}',
                    type: "POST",
                    data: {
                        query: query
                    },
                    success: function (data) {
                        $("#example1 tbody").html('');
                        $.each(data, function (index, value) {
                            var allowSale, discount, active;
                            if (value['allow_sale'] == 1) {
                                allowSale = "Có";
                                if (value['discount_type'] == 1) {
                                    discount = value['discount'] + " %";
                                } else {
                                    discount = value['discount'] + " VND";
                                }
                            } else {
                                allowSale = "Không";
                                discount = "Không";
                            }
                            if(value['is_active'] == 1){
                                active = "Có"
                            } else{
                                active = "Không"
                            }

                            $("#example1 tbody").append(`
                                        <tr>
                                        <td>${value['product_id']}</td>
                                        <td>${value['code']}</td>
                                        <td>${value['name']}</td>
                                        <td><a href="${value['id']}" title="${value['full_name']}">${value['full_name']}</a></td>
                                        <td>${value['description']}</td>
                                        <td>${value['images']}</td>
                                        <td>${value['category_name']}</td>
                                        <td>${allowSale}</td>
                                        <td>${discount}</td>
                                        <td>${value['price_base']}</td>
                                        <td>${value['price_cost']}</td>
                                        <td>${value['unit']}</td>
                                        <td><b>${active}</b></td>
                                        <td>${value['branch_name']}</td>
                                        <td>${value['inventory']}</td>
                                        <td>${value['quantity']}</td>
                                        <td>${value['created_at']}</td>
                                        <td>${value['updated_at']}</td>
                                    </tr>
                        `);
                        });
                    },
                    error: function () {
                        alert("Xảy ra lỗi. Vui lòng thử lại");
                    }
                });
            }
        });
    });
</script>
</body>
</html>
