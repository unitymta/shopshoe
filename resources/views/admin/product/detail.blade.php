<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{$pageTitle}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('bower_components/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('bower_components/Ionicons/css/ionicons.min.css')}}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('bower_components/admin-lte/dist/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('bower_components/admin-lte/dist/css/skins/_all-skins.min.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .image-show {
            width: 70px;
            height: 70px;
        }
    </style>
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

@include('admin.block.header')

<!-- Left side column. contains the logo and sidebar -->
@include('admin.block.sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Chi tiết sản phẩm
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{url('admin/product/list')}}"><i class="fa fa-dashboard"></i> Danh sách sản phẩm</a></li>
                <li class="active">Chi tiết sản phẩm</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!-- /.box-header -->
                        <form class="form-horizontal" name="updateProduct"
                              action="{{url('admin/product/update')}}/{{$getProduct->id}}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <input name="id" type="hidden" value="{{$getProduct->id}}">
                            <input name="category_id" type="hidden" value="{{$getProduct->category_id}}">
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Mã sản phẩm</th>
                                        <th>Tên</th>
                                        <th>Tên đầy đủ</th>
                                        <th>Mô tả</th>
                                        <th>Hình ảnh</th>
                                        <th>Thể loại</th>
                                        <th>Hoạt động</th>
                                        <th>Cho phép sale</th>
                                        <th>Loại giảm</th>
                                        <th>Giảm giá</th>
                                        <th>Giá bán</th>
                                        <th>Giá nhập</th>
                                        <th>Đơn vị</th>
                                        <th>Chi nhánh</th>
                                        <th>Hàng tồn kho</th>
                                        <th>Số lượng</th>
                                        <th>Ngày tạo</th>
                                        <th>Ngày cập nhật</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>{{$getProduct->product_id}}</td>
                                        <td><input name="code" type="text" value="{{$getProduct->code}}"></td>
                                        <td><input name="name" type="text" value="{{$getProduct->name}}"></td>
                                        <td><input name="full_name" type="text" value="{{$getProduct->full_name}}"></td>
                                        <td><input name="description" type="text" value="{{$getProduct->description}}"></td>
                                        <td><img class="image-show" src="{{asset('images')}}/{{$getProduct->images}}" alt="{{$getProduct->full_name}}">
                                            <input type="file" id="image" name="image" accept="image/png, image/jpeg">
                                        </td>
                                        <td>
                                            <select name="category_name" id="">
                                                @foreach($listCategoryProduct as $key=>$value)
                                                    @if($value->name == $typeCategoryProduct)
                                                        <option value="{{$value->category_id}}" selected>{{$value->name}}</option>
                                                    @else
                                                        <option value="{{$value->category_id}}">{{$value->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select></td>
                                        <td>
                                            <select name="is_active" id="">
                                                @if($getProduct->is_active==1)
                                                    <option value="1" selected>Có</option>
                                                    <option value="0">Không</option>
                                                @else
                                                    <option value="1">Có</option>
                                                    <option value="0" selected>Không</option>
                                                @endif
                                            </select>
                                        </td>
                                        <td>
                                            <select name="allow_sale" id="">
                                                @if($getProduct->allow_sale==1)
                                                    <option value="1" selected>Có</option>
                                                    <option value="0">Không</option>
                                                @else
                                                    <option value="1">Có</option>
                                                    <option value="0" selected>Không</option>
                                                @endif
                                            </select>
                                        </td>
                                        <td>
                                            <select name="discount_type" id="">
                                                @if($getProduct->discount_type==1)
                                                    <option value="1" selected>%</option>
                                                    <option value="2">VND</option>
                                                @else
                                                    <option value="1">%</option>
                                                    <option value="2" selected>VND</option>
                                                @endif
                                            </select>
                                        </td>
                                        <td>
                                            <input name="discount" type="number" value="{{$getProduct->discount}}">
                                        </td>
                                        <td><input name="price_base" type="number" value="{{$getProduct->price_base}}"></td>
                                        <td><input name="price_cost" type="number" value="{{$getProduct->price_cost}}"></td>
                                        <td><input name="unit" type="text" value="{{$getProduct->unit}}"></td>
                                        <td><input name="branch_name" type="text" value="{{$getProduct->branch_name}}"></td>
                                        <td><input name="inventory" type="number" value="{{$getProduct->inventory}}"></td>
                                        <td><input name="quantity" type="number" value="{{$getProduct->quantity}}"></td>
                                        <td>{{$getProduct->created_at}}</td>
                                        <td>{{$getProduct->updated_at}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                            <table class="table table-bordered table-striped">
                                <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <button type="submit" class="btn btn-block btn-primary btn-lg update">Cập nhật</button>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-block btn-danger btn-lg delete">Xóa</button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@include('admin.block.footer')

<!-- Control Sidebar -->
@include('admin.block.option')
<!-- /.control-sidebar -->

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('bower_components/admin-lte/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('bower_components/admin-lte/dist/js/demo.js')}}"></script>
<!-- page script -->

<script>
    $(document).ready(function () {
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.image-show').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#image").change(function () {
            readURL(this);
        });
    });
</script>
<script>
    $(function () {
        $('.box-body').css('overflow', 'scroll');

        $(".btn.delete").on("click", function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var id = $("input[name='id']").val();
            $.ajax({
                beforeSend: function (xhr, opts) {
                    if (confirm("Bạn chắc chắn muốn xóa người dùng này?")) {
                        $.ajax({
                            type: "DELETE",
                            url: '{{URL::to("/admin/product")}}/' + id,
                            success: function (data, textStatus) {
                                window.location.href = '/admin/product/list';
                            },
                            error: function (xhr) {
                                window.location.href = '/admin/product/list';
                            }
                        });
                        return true;
                    } else {
                        xhr.abort();
                    }
                }
            });
        });
    });
</script>
</body>
</html>
