<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('categories/{type}', function () {
    return view('categories');
})->name('category');
Route::get('{details}&&id={id}', 'DetailController@index')->name('detail');

// admin CMS
Route::prefix('admin')->middleware(['checkAdmin'])->group(function () {
    Route::get('/', 'Admin\DashBoardController@index');
    Route::get('/user/list', 'Admin\UserController@list');
    Route::get('/user/{id}', 'Admin\UserController@getUser');
    Route::delete('/user/{id}', 'Admin\UserController@deleteUser');
    Route::put('/user/{id}', 'Admin\UserController@updateUser')->name('updateUser');
    Route::post('user/list', 'Admin\UserController@createUser');
    Route::post('user/search', 'Admin\UserController@searchUser');

    Route::get('user/profile/{id}', 'Admin\UserProfileController@getProfile');
    Route::post('user/profile/{id}', 'Admin\UserProfileController@updateProfile');

    Route::get('product/list', 'Admin\ProductController@listProduct');
    Route::get('product/{id}', 'Admin\ProductController@getProduct');
    Route::post('product/update/{id}', 'Admin\ProductController@updateProduct');
    Route::delete('product/{id}', 'Admin\ProductController@deleteProduct');
    Route::post('product/search', 'Admin\ProductController@searchProduct');

    Route::get('category/{id}', 'Admin\CategoryController@getCategory');
});

// auth
Route::get('register', 'Auth\RegisterController@getRegister');
Route::post('register', 'Auth\RegisterController@postRegister');
Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@getLogin']);
Route::post('login', ['as' => 'login', 'uses' => 'Auth\LoginController@postLogin']);
Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\LogoutController@getLogout']);
