<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryDetail extends Model
{
    protected $table = 'category_details';

    public static function getCategoryDetails()
    {
        $categoryDetails = self::all();
        return $categoryDetails;
    }
}
