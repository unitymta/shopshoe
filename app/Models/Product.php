<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    public static function getProductDetail($productId)
    {
        $productDetail = self::where('product_id', '=', $productId)->first();
        return $productDetail;
    }

    public static function getProductNew()
    {
        $productNew = self::where('is_active', '=', 1)->join('slugs', 'products.product_id', '=', 'slugs.product_id')->orderBy('created_at', 'desc')->take(8)->get();
        return $productNew;
    }

    public static function getProductTopSeller()
    {
        $productTopSeller = self::where('is_active', '=', 1)->join('slugs', 'products.product_id', '=', 'slugs.product_id')->orderByRaw('quantity - inventory DESC')->take(8)->get();
        return $productTopSeller;
    }

    public static function getProductFeature()
    {
        $productFeature = self::where('is_active', '=', 1)->join('slugs', 'products.product_id', '=', 'slugs.product_id')->orderByRaw('quantity - inventory ASC')->take(8)->get();
        return $productFeature;
    }

    public static function getProductRelationships($productDetail)
    {
        $productRelationships = self::where([['category_id', '=', $productDetail->category_id], ['product_id', '!=', $productDetail->product_id]])->take(3)->get();
        return $productRelationships;
    }

    // Admin CMS
    public static function getProductListCms()
    {
        $getProductList = self::get();
        return $getProductList;
    }

    public static function getProductDetailCms($id)
    {
        $getProductDetail = self::where('id', '=', $id)->first();
        return $getProductDetail;
    }

    public static function getListCategoryProductCms()
    {
        $listCategoryProduct = Category::get();
        return $listCategoryProduct;
    }

    public static function getTypeCategoryProductCms($id)
    {
        $productDetail = self::getProductDetailCms($id);
        if ($productDetail) {
            $typeProduct = $productDetail->category_id;
        } else {
            return redirect('/admin/product/list');
        }

        $productTypeCategory = Category::where('category_id', '=', $typeProduct)->first();
        return $productTypeCategory->name;
    }

    public static function updateProductCms($id, $request, $images)
    {
        if ($images != null) {
            $updateProduct = self::where('id', '=', $id)->update(array(
                'code' => $request->get('code'),
                'name' => $request->get('name'),
                'full_name' => $request->get('full_name'),
                'description' => $request->get('description'),
                'images' => $images,
                'category_id' => $request->get('category_id'),
                'allow_sale' => $request->get('allow_sale'),
                'discount_type' => $request->get('discount_type'),
                'discount' => $request->get('discount'),
                'price_base' => $request->get('price_base'),
                'price_cost' => $request->get('price_cost'),
                'unit' => $request->get('unit'),
                'is_active' => $request->get('is_active'),
                'branch_name' => $request->get('branch_name'),
                'inventory' => $request->get('inventory'),
                'quantity' => $request->get('quantity'),
            ));
            return $updateProduct;
        } else {
            $updateProduct = self::where('id', '=', $id)->update(array(
                'code' => $request->get('code'),
                'name' => $request->get('name'),
                'full_name' => $request->get('full_name'),
                'description' => $request->get('description'),
                'category_id' => $request->get('category_id'),
                'allow_sale' => $request->get('allow_sale'),
                'discount_type' => $request->get('discount_type'),
                'discount' => $request->get('discount'),
                'price_base' => $request->get('price_base'),
                'price_cost' => $request->get('price_cost'),
                'unit' => $request->get('unit'),
                'is_active' => $request->get('is_active'),
                'branch_name' => $request->get('branch_name'),
                'inventory' => $request->get('inventory'),
                'quantity' => $request->get('quantity'),
            ));
            return $updateProduct;
        }

    }

    public static function deleteProduct($id)
    {
        $deleteProduct = self::where('id', '=', $id)->delete();
        return $deleteProduct;
    }

}
