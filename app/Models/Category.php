<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    public static function getCategory()
    {
        $categories = self::all();
        return $categories;
    }

    public static function getProductOfCategoryId($id)
    {
//        $productTopSeller = self::where('is_active', '=', 1)->join('slugs', 'products.product_id', '=', 'slugs.product_id')->orderByRaw('quantity - inventory DESC')->take(8)->get();
        $getProduct = self::where('category_id', '=', $id)->join('products', 'categories.category_id', '=', 'products.category_id')->get();
        return $getProduct;
    }
}
