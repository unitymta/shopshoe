<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Slug extends Model
{
    protected $table = 'slugs';

    public static function getSlugCategory($productId)
    {
        $slugCategory = self::where('product_id', '=', $productId)->first()->slug_category;
        return $slugCategory;
    }

    public static function deleteSlugProduct($id)
    {
//        Schema::table('slugs', function (Blueprint $table) {
//            $table->dropForeign('slugs_product_id_foreign');
//        });

        $deleteSlug = self::where('product_id', '=', $id)->delete();
        return $deleteSlug;
    }
}
