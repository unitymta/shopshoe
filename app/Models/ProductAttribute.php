<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    protected $table = 'product_attributes';

    public static function getProductAttributes($productId)
    {
        $productAttributes = self::where('product_id', '=', $productId)->get();
        return $productAttributes;
    }
}
