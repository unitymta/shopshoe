<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\CategoryDetail;
use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $categories = Category::getCategory();
        $categoryDetails = CategoryDetail::getCategoryDetails();
        $productNew = Product::getProductNew();
        $productTopSeller = Product::getProductTopSeller();
        $productFeature = Product::getProductFeature();

        return view('home',
            [
                'categories' => $categories,
                'categoryDetails' => $categoryDetails,
                'productNew' => $productNew,
                'productTopSeller' => $productTopSeller,
                'productFeature' => $productFeature
            ]
        );
    }
}
