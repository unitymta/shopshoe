<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\CategoryDetail;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\Slug;

class DetailController extends Controller
{
    public function index($slug, $productId)
    {
        $categories = Category::getCategory();
        $categoryDetails = CategoryDetail::getCategoryDetails();
        $productDetail = Product::getProductDetail($productId);
        $slugCategory = Slug::getSlugCategory($productId);
        $productAttributes = ProductAttribute::getProductAttributes($productId);
        $productRelationships = Product::getProductRelationships($productDetail);

        $jsonString = file_get_contents(base_path('resources/json/local.json'));
        $dataLocal = json_decode($jsonString, true);

        return view('details',
            [
                'categories' => $categories,
                'categoryDetails' => $categoryDetails,
                'productDetail' => $productDetail,
                'slug' => $slug,
                'slugCategory' => $slugCategory,
                'productAttributes' => $productAttributes,
                'dataLocal' => $dataLocal,
                'productRelationships' => $productRelationships
            ]
        );
    }
}
