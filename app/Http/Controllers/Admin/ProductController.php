<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Slug;

class ProductController extends Controller
{
    public function listProduct()
    {
        $getProductList = Product::getProductListCms();
        return view('admin.product.list', ['pageTitle' => 'Product List', 'getProductList' => $getProductList]);
    }

    public function getProduct($id)
    {
        if (Product::FindOrFail($id)) {
            $getProduct = Product::getProductDetailCms($id);
            $typeCategoryProduct = Product::getTypeCategoryProductCms($id);
            $listCategoryProduct = Product::getListCategoryProductCms();
            return view('admin.product.detail',
                ['pageTitle' => 'Product Detail', 'getProduct' => $getProduct, 'typeCategoryProduct' => $typeCategoryProduct, 'listCategoryProduct' => $listCategoryProduct]);
        } else {
            return redirect('/admin/product/list');
        }

    }

    public function searchProduct(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $data = Product::where('full_name', 'LIKE', "%{$query}%")->get();
            return $data;
        }
    }

    public function updateProduct($id, Request $request)
    {
        $data = Product::FindOrFail($id);
        if (request()->image != null) {
            request()->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . "-id" . $id . '.' . request()->image->getClientOriginalExtension();
            if (file_exists('images/' . $data->images) && !empty($data->images)) {
                unlink('images/' . $data->images);
                Product::where('id', '=', $id)->update(array('images' => ''));
            }
            request()->image->move(public_path('images'), $imageName);
            Product::updateProductCms($id, $request, $imageName);
        } else {
            Product::updateProductCms($id, $request, null);
        }
        return back();
    }

    public function deleteProduct($id)
    {
        $slug = Slug::deleteSlugProduct($id);
        $product = Product::deleteProduct($id);
        if ($slug || $product) {
            Slug::deleteSlugProduct($id);
            Product::deleteProduct($id);
            return true;
        } else {
            return true;
        }
    }
}
