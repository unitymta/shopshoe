<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\File;

class UserProfileController extends Controller
{
    public function getProfile($id, Request $request)
    {
        $getUserProfile = User::getUserProfile($id);
        return view('admin.user.profile', ['pageTitle' => 'Profile User', 'getUserProfile' => $getUserProfile]);
    }

    public function updateProfile($id, Request $request)
    {
        $data = User::FindOrFail($id);
        if (request()->image != null) {
            request()->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time() . "-id" . $id . '.' . request()->image->getClientOriginalExtension();
            if (file_exists('images/avatar/' . $data->image) && !empty($data->image)) {
                unlink('images/avatar/' . $data->image);
                User::where('id', '=', $id)->update(array('image' => ''));
            }
            request()->image->move(public_path('images/avatar'), $imageName);
            User::updateProfile($id, $request, $imageName);
        } else {
            User::updateProfile($id, $request, null);
        }
        return back();
    }
}
