<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Session;

class UserController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data,
            [
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:8'
            ],
            [
                'email.required' => 'Email là trường bắt buộc',
                'email.email' => 'Email không đúng định dạng',
                'email.max' => 'Email không quá 255 ký tự',
                'email.unique' => 'Email đã tồn tại',
                'password.required' => 'Mật khẩu là trường bắt buộc',
                'password.min' => 'Mật khẩu phải chứa ít nhất 8 ký tự',
            ]
        );
    }

    protected function create(array $data)
    {
        return User::createUser($data);
    }

    public function list()
    {
        request()->get('type') == null ? $type = '' : $type = request()->get('type');

        $count = User::listCount();
        $listAdmin = User::listType($type);

        return view('admin.user.list',
            [
                'pageTitle' => 'Admin List User',
                'listAdmin' => $listAdmin,
                'count' => $count
            ]
        );
    }

    public function getUser($id)
    {
        $getUser = User::getUser($id);
        return $getUser;
    }

    public function deleteUser($id)
    {
        $deleteUser = User::deleteUser($id);
        return $deleteUser;
    }

    public function updateUser($id, Request $request)
    {
        $updateUser = User::updateUser($id, $request);
        return $updateUser;
    }

    public function createUser(Request $request)
    {
        $allRequest = $request->all();
        $validator = $this->validator($allRequest);

        if (!$validator->fails()) {
            $this->create($allRequest);
        } else {
            return redirect('admin/user/list');
        }
    }

    public function searchUser(Request $request)
    {
        $typeUser = $request->get('typeUser');

        if ($request->get('query')) {
            $query = $request->get('query');
            if ($typeUser == 'admin') {
                return $data = User::where('given_name', 'LIKE', "%{$query}%")->where('level', '=', 2)->get();
            }
            if ($typeUser == 'user') {
                return $data = User::where('given_name', 'LIKE', "%{$query}%")->where('level', '=', 3)->get();
            }
            if ($typeUser == 'block') {
                return $data = User::where('given_name', 'LIKE', "%{$query}%")->where('level', '=', 4)->get();
            }
            if ($typeUser == '') {
               $data = User::where('given_name', 'LIKE', "%{$query}%")->where('level', '!=', 1)->get();
               return $data;
            }
        }
    }
}
