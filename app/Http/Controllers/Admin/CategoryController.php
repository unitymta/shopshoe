<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function getCategory($id)
    {
        $getProductOfCategory = Category::getProductOfCategoryId($id);
        return view('admin.category.category', ['getProductOfCategory' => $getProductOfCategory]);
    }
}
