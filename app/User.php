<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Self_;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'email',
        'password',
        'level',
        'given_name',
        'address',
        'mobile_phone',
        'birthday'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function listCount()
    {
        $count = [];
        $countAdmin = self::where('level', '=', '2')->count();
        $countUser = self::where('level', '=', '3')->count();
        $countBlock = self::where('level', '=', '4')->count();
        $countAll = self::where('level', '!=', '1')->count();

        array_push($count, (object)[
            'countAll' => $countAll,
            'countAdmin' => $countAdmin,
            'countUser' => $countUser,
            'countBlock' => $countBlock
        ]);
        return $count;
    }

    public static function listType($type)
    {
        if ($type == 'admin') {
            return $listAdmin = self::where('level', '=', '2')->paginate(10);
        }
        if ($type == 'user') {
            return $listAdmin = self::where('level', '=', '3')->paginate(10);
        }
        if ($type == 'block') {
            return $listAdmin = self::where('level', '=', '4')->paginate(10);
        }
        if ($type == '') {
            return $listAdmin = self::where('level', '!=', '1')->paginate(10);
        }
    }

    public static function getUser($id)
    {
        $getUser = self::where('id', '=', $id)->get();
        return $getUser;
    }

    public static function deleteUser($id)
    {
        $deleteUser = self::where('id', '=', $id)->delete();
        return $deleteUser;
    }

    public static function updateUser($id, $request)
    {
        $updateUser = self::where('id', '=', $id)->update(array(
            'given_name' => $request->get('name'),
            'address' => $request->get('address'),
            'mobile_phone' => $request->get('phone'),
            'email' => $request->get('email'),
            'birthday' => $request->get('birthday')
        ));
        return $updateUser;
    }

    public static function createUser($data)
    {
        $saveUser = new User;
        $lastUser = self::latest('user_id')->first();
        $saveUser->user_id = $lastUser->user_id + 3;
        $saveUser->email = $data['email'];
        $saveUser->password = bcrypt($data['password']);
        $saveUser->given_name = $data['name'];
        $saveUser->address = $data['address'];
        $saveUser->mobile_phone = $data['phone'];
        $saveUser->birthday = $data['birthday'];
        $saveUser->save();

    }

    public static function getUserProfile($id)
    {
        $userProfile = self::where('id', '=', $id)->first();
        return $userProfile;
    }

    public static function updateProfile($id, $request, $imageName)
    {
        if ($imageName != null) {
            $updateProfile = self::where('id', '=', $id)->update(array(
                'given_name' => $request->get('name'),
                'address' => $request->get('address'),
                'mobile_phone' => $request->get('phone'),
                'email' => $request->get('email'),
                'description' => $request->get('description'),
                'birthday' => $request->get('birthday'),
                'image' => $imageName
            ));
            return $updateProfile;
        } else {
            $updateProfile = self::where('id', '=', $id)->update(array(
                'given_name' => $request->get('name'),
                'address' => $request->get('address'),
                'mobile_phone' => $request->get('phone'),
                'email' => $request->get('email'),
                'description' => $request->get('description'),
                'birthday' => $request->get('birthday')
            ));
            return $updateProfile;
        }
    }
}
