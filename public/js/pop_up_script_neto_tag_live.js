// /*global console, $*/
// $(function() {
//     "use strict";
//     $.get("https://go.smartrmail.com/pop_ups/get_settings", {
//         domain: NETO.systemConfigs['domain']
//     }).done(function(data) {
//         var currentDate = new Date(), time = getCookie("sm_popup_show");
//         var d = time.length > 0 ? new Date(time) : new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - 1);
//         var showOnMobileAndDesktop = (mobileScreen() && data.show_on_mobile) || !mobileScreen()
//         if (data.enabled && getCookie("sm_popup_submitted") != "true" && currentDate > d) {
//             $("body").append(data.pop_up_html);
//             $("body").append(data.pop_up_js);
//             if (showOnMobileAndDesktop) {
//                 applyMobileView();
//                 var timeout = data.trigger_time.split("_");
//                 if (timeout[0] == "immediately") {
//                     $(".sm_popup").fadeIn("slow");
//                 }else if (timeout[2] == 'click') {
//                     var clickCount = 0;
//                     $("body").on('click', function(){
//                         clickCount++;
//                         if (clickCount == timeout[0]) {
//                             $(".sm_popup").fadeIn("slow");
//                         }
//                     });
//                 }else if (timeout[1] == "seconds") {
//                     setTimeout(function() {
//                         $(".sm_popup").fadeIn("slow");
//                     }, timeout[0] * 1000);
//                 } else {
//                     setTimeout(function() {
//                         $(".sm_popup").fadeIn("slow");
//                     }, data.custom_trigger_time * 1000);
//                 }
//             } else {
//                 return;
//             }
//         }
//     });
//
//     function applyMobileView(){
//         if (mobileScreen()) {
//             $('.sm_popup').removeClass('small-screen large-screen full-screen').css({
//                 'height': '370px',
//                 'margin': '-187.5px -138px',
//                 'width': '280px',
//                 'left': '50%'
//             });
//         }
//     }
//
//     function mobileScreen(){
//         return window.matchMedia("screen and (max-width: 480px)").matches;
//     }
//
//     function getCookie(cname) {
//         var name = cname + "=";
//         var ca = document.cookie.split(';');
//         for (var i = 0; i < ca.length; i++) {
//             var c = ca[i];
//             while (c.charAt(0) == ' ') c = c.substring(1);
//             if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
//         }
//         return "";
//     }
// });
