$(".after-search").click(function() {
    $(this).removeClass("active");
    $(".customsearchbar").removeClass("active");
});
$(".icon-search-mobile").click(function(e) {
    $(".customsearchbar").toggleClass("active");
    $(".after-search").toggleClass("active");
});
$("#left-sidebar").before("<div class='overlay-bg'></div>");
$("#filters").prepend('<span class="close"></div>');
$(".icon-filter").click(function() {
    $("#left-sidebar").addClass("active");
    $(".overlay-bg").addClass("active");
});
$("#filters .close").click(function() {
    $("#left-sidebar").removeClass("active");
    $(".overlay-bg").removeClass("active");
});
$(".overlay-bg").click(function() {
    $(this).removeClass("active");
    $("#left-sidebar").removeClass("active");
});
$(".icon-click").click(function() {
    var ele1 = $(this).attr("data-click");
    var ele2 = $(ele1);
    ele2.toggleClass("active");
    ele2.prev(".after-div").toggleClass("active");
});
$(".after-div").click(function() {
    $(this).removeClass("active");
    $(".data-click").removeClass("active");
});
$(".data-click .close").click(function() {
    $(".after-div").removeClass("active");
    $(".data-click").removeClass("active");
});
$("#menu-mobile .navbar-nav > li .dropdown-menu").parent("li").addClass("hassub");
$("#menu-mobile .navbar-nav > li.hassub > a").after("<div class='after-div-lv2'></div>");
$("#menu-mobile .navbar-nav > li.hassub .after-div-lv2").click(function() {
    $(this).parents(".hassub").toggleClass("sub-transition");
    $("#menu-mobile").toggleClass("sub-transition");
});
$('#menu-mobile .navbar-nav > li.hassub').click(function() {
    $(this).children(".dropdown-menu").slideToggle();
    $(this).toggleClass("active");
});
$(".browsinghistory .tooltip").click(function() {
    $(this).parent().toggleClass("active");
});
$(".browsinghistory").click(function(e) {
    e.stopPropagation();
});
$("body").click(function() {
    $(".browsinghistory").removeClass("active");
});
$('.cart-qty-box .form-control').focusout(function() {
    javascript: document.checkout.submit();
});
$('.youmaylike .thumbnail .buying-options label[for="check_addtocart"]').click(function() {
    $(this).parent().find("button.addtocart").click();
});
$(".title-menu-cat").click(function() {
    $(".navbar-responsive-collapse").toggleClass("active");
});
$('.add').click(function() {
    if ($(this).prev().val() < 99) {
        $(this).prev().val(+$(this).prev().val() + 1);
    }
});
$('.sub').click(function() {
    if ($(this).next().val() > 0) {
        if ($(this).next().val() >= 0) $(this).next().val(+$(this).next().val() - 1);
        if ($('.cart-qty-box .form-control').val() != 0) {
        }
    }
});
$(".customshoppingcart22").parents(".categorypage").addClass("wrap-cart-page");
$('.add-discount').click(function() {
    $('.subtotaltable .table.grand-total-table tr:first-child + tr').slideToggle();
})
var htmlFlagimg = '';
$('.nav-actcurrency .dropdown-menu > li > a').click(function() {
    htmlFlagimg = $(this).html() + '<i class="fa fa-chevron-down"></i>';
    console.log(htmlFlagimg);
    if (typeof(Storage) !== "undefined") {
        localStorage.setItem("lastname", htmlFlagimg);
    } else {
        document.getElementById("result").innerHTML = "Sorry, your browser does not support Web Storage...";
    }
});
var x = localStorage.getItem("lastname");
if (x != null) {
    $('#top-navbar .nav-currency.btn-group > button').html(x);
    $('#menu-mobile .head .topmenucustom .nav-currency.btn-group > button').html(x)
}
var initialLoad = true;
$(document).ready(function() {
    $('.nav-actcurrency .dropdown-menu > li > a').click(function() {
        var htmlFlagimg = $(this).html() + '<i class="fa fa-chevron-down"></i>';
        console.log(htmlFlagimg);
        $('#top-navbar .nav-currency.btn-group > button').html(htmlFlagimg);
        $('#menu-mobile .head .topmenucustom .nav-currency.btn-group > button').html(htmlFlagimg);
    });
    $('td#recent_sales_content_description a').each(function() {
        var product_Name = $(this).text().slice(0, 80);
        $(this).text(product_Name);
    });
});
if ($('.fixed-top-price').length > 0) {
    $(document).scroll(function() {
        var top = $("#n_product .custompagerow2").offset().top;
        var height = $("#n_product .custompagerow2").height();
        var bottom = top + height;
        if ($(this).scrollTop() >= $(".tabcustom").offset().top) {
            $(".fixed-top-price").addClass("active");
        } else {
            $(".fixed-top-price").removeClass("active");
        }
        if ($(this).scrollTop() > bottom) {
            $(".fixed-top-price").removeClass("active");
        }
    });
}
$(document).ready(function() {
    var rrpTotal = 0;
    $(".categorypage.wrap-cart-page .text-right.priceshop").each(function(index) {
        rrpTotal += Number($(this).children('.cart-rrp').val());
    });
    rrpTotal = parseFloat(rrpTotal.toFixed(2));
    if (rrpTotal > 0) {
        var priceSave = parseFloat(Number(rrpTotal - $("#cart-subtotal").val())).toFixed(2);
        $(".subtotaltable .table tr.total_rrp td .total-rrp").text("$" + rrpTotal);
        $(".subtotaltable .table tr.total_save td .tottal-save").text("$" + priceSave);
        var currencytext = "";
        currencytext = $(".nav-currency.btn-group > button > img + span").html();
    } else {
        $(".subtotaltable .table tr.total_rrp").remove();
    }
});
$(window).on('load resize', function() {
    if ($(window).width() <= 1199) {
        $("#cartcontentsheader").click(function() {
            window.location.href = $(this).attr("data-href");
        });
    }
});
